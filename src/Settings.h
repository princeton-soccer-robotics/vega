#pragma once
#include <Arduino.h>

using namespace std;

/**
 * SPI CS pin for line ADC 1
 **/
#define LINE_SPI_CS_PIN_1 10
/**
 * SPI CS pin for line ADC 2
 **/
#define LINE_SPI_CS_PIN_2 0
/**
 * SPI CS pin for ball ADC 1
 **/
#define BALL_SPI_CS_PIN_1 38
/**
 * SPI CS pin for ball ADC 2
 **/
#define BALL_SPI_CS_PIN_2 37
/**
 * SPI CS pin for ball ADC 3
 **/
#define BALL_SPI_CS_PIN_3 36
/**
 * Software Switch pin
 **/
#define SOFTWARE_SWITCH_PIN A16
/**
 * Reset switcj
 **/
#define RESET_SWITCH_PIN A12
/**
 * The baud rate of the XBee
 **/
#define XBEE_BAUD_RATE 9600
/**
 * The frequency at which motors are written data
 **/
#define MOTOR_FREQUENCY 93750
/**
 * The amount of separate values being received from the camera
 **/
#define CAMERA_COMM_DATA 3
/**
 * The amount of line sensors on the robot
 **/
#define LINE_SENSORS 16
/**
 * The amount of ball sensors on the robot
 **/
#define BALL_SENSORS 24
/**
 * The threshold for line detection
 **/
#define LINE_THRESHOLD 300
/**
 * The filter for insanely large line sensor values
 **/
#define LINE_BIG_FILTER 900
/**
 * The filter for insanely small line sensor values
 **/
#define LINE_SMALL_FILTER 20  
/**
 * Amount of line sensors that need to trigger to assume pickup
 **/
#define PICKUP_THRESHOLD 10 //was 10
/**
 * gradient at which the centeriong is intensified
 * or something like that
 **/
#define CENTER_MULTIPLIER 0.8
/**
 * pin for the light gate (which is used to detect ball)
 * TODO : MAKE THIS A REAL PIN!!!
 **/
#define LIGHT_GATE_PIN A17
/**
 * 
 **/
#define CALIBRATION_SWITCH_PIN A12 
/**
 * The threshold for the light gate to trigger
 **/
#define LIGHT_GATE_THRESHOLD 820//825
/**
 * The power of the motors (in percents)
 **/
#define MOVEMENT_POWER 45
/**
 * Any ball sensor under this value will be ommitted from calculcations
 **/
#define BALL_VALUE_FILTER 400
/**
 * 
 **/
#define LINE_AVERAGING_COUNT 3
/**
 * ms for how long 
 **/
#define CAMERA_TIMEOUT 300
/**
 * the default float value from camera
 **/
#define GoalieSensitivity 2
/**
 * the default float value from camera
 **/
#define DEFAULT_CAMERA_VALUE 69420.00

#define CENTERING_AGRESSIVENESS 0

#define OLD_KOIG false
/**
 * Toggleable providers
 **/
#define ENABLE_BALL_SENSOR_PROVIDER
#define ENABLE_COMPASS_PROVIDER_PROVIDER
//#define ENABLE_LIGHT_GATE_PROVIDER
#define ENABLE_LINE_SENSOR_PROVIDER
#define ENABLE_MOVEMENT_PROVIDER
//#define ENABLE_CAMERA_PROVIDER
//#define ENABLE_KICKER_PROVIDER
//#define ENABLE_BLUETOOTH_PROVIDER
#define ENABLE_DRIBBLER_PROVIDER
/**
 * Toggleable processes
 **/
#define ENABLE_LINE_PROCESS
#define ENABLE_CENTERING_PROCESS
//#define ENABLE_FOLLOW_PROCESS
//#define ENABLE_GOAL_PROCESS
#define ENABLE_LIGHT_GATE_PROCESS
//#define ENABLE_GOALIE_PROCESS
#define ENABLE_TC1

