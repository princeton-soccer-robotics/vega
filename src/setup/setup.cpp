#include "setup.h"

void Setup::scanForI2C() {
  Wire.begin();
  byte found = 0;
  PSerial.println("Scanning for available I2C Devices...");
  for (byte address = 1; address < 127; address++) {
    Wire.beginTransmission(address);
    byte error = Wire.endTransmission();
    if (error == 0) {
      found++;
      PSerial.printf("I2C Device Found At Address 0x%i\n", address);
    }
  }
  PSerial.printf("%i I2C Devices Found!\n", found);
}