#include <framework/TaskManagement.h>

ThreadController mainController = ThreadController();

void makeTask(void (*callback)(void)) {
  auto t = new Thread(callback);
  mainController.add(t);
}