#pragma once
#include <Thread.h>
#include <ThreadController.h>

extern ThreadController mainController;

void makeTask(void (*callback)(void));

#define VEGA_CREATE_TASK() makeTask(handler)