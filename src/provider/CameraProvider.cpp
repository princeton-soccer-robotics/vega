#include "provider/CameraProvider.h"
#include <Polaris.h>
#include "provider/CompassProvider.h"

polaris::Vec2 CameraProvider::goalVec = polaris::Vec2(0, 0);
std::string CameraProvider::buffer = "";

int CameraProvider::lastSend = 0;
int CameraProvider::lastFriendlySend = 0;
int CameraProvider::lastEnemySend = 0;

float CameraProvider::friendlyDist = 260;
float CameraProvider::friendlyAngle = 0;
float CameraProvider::enemyDist = 0;
float CameraProvider::enemyAngle = 0;
bool CameraProvider::seesFriendly = false;
int CameraProvider::preventCrashes = 0;
float CameraProvider::selfX = 0;
float CameraProvider::selfY = 0;

void CameraProvider::make() {
  Serial2.begin(19200);
  VEGA_CREATE_TASK();
}

float CameraProvider::nextFloat(std::istringstream& split) {
  std::string item;
  float tmp;  // used to temporarily store cam values in case it is 69420
  std::getline(split, item, '|');
  tmp = atof(item.c_str());
  return tmp;
}

void CameraProvider::handler() {
  preventCrashes = 1;
  //Serial.println("Camera handler is being run");
  if (Serial2.available() > 0) {
    //Serial.println("Serial2 is available");
    bool readComplete = false;
    for (int i = 0; i < Serial2.available(); i++) {
      char read = Serial2.read();
      if (read == '\n' && buffer.length() != 0) {
        readComplete = true;
      } else {
        buffer += read;
      }
    }
    if (readComplete) {
      //Serial.println("checking");
      std::istringstream split(buffer);
      friendlyAngle = nextFloat(split);
      friendlyDist = nextFloat(split);
      enemyAngle = nextFloat(split);
      enemyDist = nextFloat(split);
      //Serial.printf("camera data recv : %f %f %f %f \r\n", friendlyAngle, friendlyDist, enemyAngle, enemyDist);
      if (
        !polaris::areEqual(friendlyAngle, DEFAULT_CAMERA_VALUE) && 
        !polaris::areEqual(friendlyDist, DEFAULT_CAMERA_VALUE)
      ) {
        lastFriendlySend = millis();
        lastSend = millis();
      }

      if (
        !polaris::areEqual(enemyAngle, DEFAULT_CAMERA_VALUE) && 
        !polaris::areEqual(enemyDist, DEFAULT_CAMERA_VALUE)
      ) {
        lastEnemySend = millis();
        lastSend = millis();
      }

      // Serial.println(friendlyDist);
      goalVec =
          polaris::AngleDeg{
              -1.0 * (polaris::AngleRad{goalie ? friendlyAngle : enemyAngle}.toDeg().value + 90)}
              .toVec()
              .toUnitVector();
      //Serial.println(goalVec.toString());
      buffer = "";
      // Serial.println(String(millis() - lastSend));
      //Serial.println("freidnangle from cam process" + String(friendlyAngle));

      //float angOffset = M_PI / 180 * (450 - CompassProvider::rotation);

      // if (friendlyDist == 69420 || friendlyAngle == 69420){
      //   // base location on enemy
      //   selfX = -enemyDist * cos(enemyAngle);
      //   selfY = -enemyDist * sin(enemyAngle) + 38;
      //   Serial.println("using enemy" + String(friendlyDist) + "   " + String(friendlyAngle) +  "    " + String(selfX) + "," + String(selfY));
      // } 
      //else if (enemyDist == 69420 || enemyAngle == 69420){
        // base location on friend
        selfX = -friendlyDist * cos(friendlyAngle + M_PI) + 6;
        selfY = -38 - friendlyDist * sin(friendlyAngle + M_PI);
       // Serial.println("using friend" + String(friendlyDist) + "   " + String(friendlyAngle) +  "    " + String(selfX) + "," + String(selfY));
      // }
      // else {
      //   //average both
      //   selfX = -0.5 * (-enemyDist * cos(enemyAngle) + -friendlyDist * cos(friendlyAngle));
      //   selfY = 0.5 * (-enemyDist * sin(enemyAngle) + 38 + -38 - friendlyDist * sin(friendlyAngle));
      // }
      //Serial.println(String(selfX) + " X, " + String(selfY) + "is Y.");
      //Serial.println(String(friendlyDist) + " " + String(enemyDist));
    }
  }
}