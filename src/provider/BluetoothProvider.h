#pragma once

#include <Polaris.h>
#include <XBee.h>
#include <framework/TaskManagement.h>
#include <string>
#include "Settings.h"
#include "protocol/intercom/intercom.pb-c.h"
#include <sstream>
#include <string>
#include <Polaris.h>
#include "process/globals.h"
#include "provider/BallSensorProvider.h"

struct BluetoothProvider {
  static std::string data;
  static std::string buffer;
  static uint32_t time;
  static int preventsCrashes;
  static int lastSend;

  static void make();

  static void handler();
};  // struct BluetoothProvider