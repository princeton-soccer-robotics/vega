#include "provider/CompassProvider.h"

int CompassProvider::taskHandle = 0;
Adafruit_BNO055 CompassProvider::bno;
float CompassProvider::rotation = 0;
float CompassProvider::targetRotation = 0;

void CompassProvider::make() {
  bno = Adafruit_BNO055(55);
  if (!bno.begin()) {
    Serial.println("COMPASS STUCK COMPASS STUCK AHH");
    while (1) {
    }
  }
  bno.setExtCrystalUse(true);
   uint8_t system, gyro, accel, mag;
  while (system != 3)
  {
    Serial.printf("It's not fully calibrated : %d, %d, %d %d\r\n", system, gyro, accel, mag);
    bno.getCalibration(&system, &gyro, &accel, &mag);
    delay(500);
  }
  MovementProvider::motors.frontRight.setPower(40);
  MovementProvider::motors.frontRight.write();
  delay(300);
  MovementProvider::motors.frontRight.setPower(0);
  MovementProvider::motors.frontRight.write();
  VEGA_CREATE_TASK();
}

void CompassProvider::setOrigin() {
  sensors_event_t ev;
  bno.getEvent(&ev);
  targetRotation = ev.orientation.roll;
  Serial.println("setting origin to " + String(targetRotation));
}

void CompassProvider::handler() {
  sensors_event_t ev;
  bno.getEvent(&ev);
  rotation = ev.orientation.roll;
}