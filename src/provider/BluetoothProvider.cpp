#include "provider/BluetoothProvider.h"
#include "process/challenge/TC1.h"

std::string BluetoothProvider::data = "";
std::string BluetoothProvider::buffer = "";
uint32_t BluetoothProvider::time = 0;
int BluetoothProvider::lastSend = 0;

void BluetoothProvider::make() {
  Serial3.begin(9600);
  VEGA_CREATE_TASK();
}

void BluetoothProvider::handler() {
  if (TC1PROCESS::isSwapNow){
    Serial3.println("swap");
    TC1Process::isSwapNow = false;
  }
  if (Serial3.available() > 0) {
    bool readComplete = false;
    for (int i = 0; i < Serial3.available(); i++) {
      char read = Serial3.read();
      if ((read == '\n' || read == '\r') && buffer.length() != 0) {
        readComplete = true;
      } else {
        buffer += read;
      }
    }
    if (readComplete) {
      std::istringstream split(buffer);
      std::string item;
      std::getline(split, item, '|');
      Serial.println("just get roleswapped");
      buffer = "";
      TC1Process::isSwapNow = true;
    }
  }
}
