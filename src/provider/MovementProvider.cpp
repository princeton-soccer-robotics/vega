#include "provider/MovementProvider.h"
#include "process/offense/CenteringProcess.h"
#include "process/defense/GoalieProcess.h"
#include "process/LineProcess.h"


int MovementProvider::taskHandle = 0;

polaris::Vec2 MovementProvider::moveVec = polaris::Vec2(0, 0);
polaris::Vec2 MovementProvider::projectionVec = polaris::Vec2(0, 0);
float MovementProvider::ProportionOfMaxSpeed = 1;
unordered_map<int, std::pair<VecPriority, polaris::Vec2>>
    MovementProvider::processVecs =
        unordered_map<int, std::pair<VecPriority, polaris::Vec2>>();
int MovementProvider::power = MOVEMENT_POWER;
float MovementProvider::rotation = 0.0f;
Motors MovementProvider::motors = Motors{
  frontRight : Motor(33, 22),
  frontLeft : Motor(3, 1),
  backRight : Motor(9, 6),
  backLeft : Motor(5, 4)
};

Motor::Motor(int dirPin, int powPin) {
  this->dirPin = dirPin;
  this->powPin = powPin;
  analogWriteFrequency(dirPin, MOTOR_FREQUENCY);
  analogWriteFrequency(powPin, MOTOR_FREQUENCY);
}

void Motor::setPower(int power) {
  this->power = min(((float)power / 100) * 204, 255);
}

void Motor::write() {
  analogWrite(this->powPin, abs(this->power));
  analogWrite(this->dirPin, this->power < 0 ? 255 : 0);
}

void MovementProvider::make() {
  VEGA_CREATE_TASK();
  Serial.println("Movement provider task started");
  ProportionOfMaxSpeed = 1;
}

void MovementProvider::handler() {
  MovementProvider::recalculate();
  motors.frontRight.write();
  motors.frontLeft.write();
  motors.backRight.write();
  motors.backLeft.write();
}

void MovementProvider::setPower(int power) {
  MovementProvider::power = power;
}

void MovementProvider::setVec(ProcessID id, polaris::Vec2 vec) {
  MovementProvider::processVecs[id].second = vec;
}

void MovementProvider::setPrio(ProcessID id, VecPriority prio) {
  MovementProvider::processVecs[id].first = prio;
}

void MovementProvider::recalculate() {
  // recalculate the moveVec based on the vectors and respective priorities
  moveVec = polaris::Vec2(0, 0);
  bool flag = false;
  for (auto& process : MovementProvider::processVecs) {
    switch (process.second.first) {
      case VecPriority::LOW_PRIO: {
        moveVec += process.second.second * 0.5;
        break;
      };
      case VecPriority::MEDIUM_PRIO: {
        moveVec += process.second.second;
        break;
      }
      case VecPriority::HIGH_PRIO: {
        moveVec += process.second.second * 2;
        break;
      }
      case VecPriority::FULL_PRIO: {
        if (!process.second.second.isZero()) {
          moveVec = process.second.second;
          flag = true;
        }
        break;
      }
    }
    if (flag)
      break;
  }
  // Serial.println("movevec: " + moveVec.toString());
  moveVec = moveVec.toUnitVector();
  // if (!moveVec.isZero()) {
  //   auto dotProd = polaris::dotProduct(moveVec, projectionVec);
  //   // Serial.println("MoveVec: { " + String(moveVec.x) + ", " +
  //   //                String(moveVec.y) + "} ProjectionVec: { " +
  //   //                String(projectionVec.x) + ", " + String(projectionVec.y) +
  //   //                " } dotProd: " + String(dotProd));
  //   if (!projectionVec.isZero() && dotProd < 0) {
  //     auto projectedProjectionVec = (projectionVec.toAngleDeg() + 90.0).toVec();
  //     auto realDotProd = polaris::dotProduct(moveVec, projectedProjectionVec);
  //     moveVec = projectedProjectionVec * realDotProd;
  //   }
  // }
  // Serial.println("MoveVec Projected: { " + String(moveVec.x) + ", " +
  //                String(moveVec.y) + "} ");
  rotation += TC1Process::rotationoffset;
  const float fixedRotation = 1.3 * MovementProvider::rotation / 180.0;
  auto movementAngle = moveVec.toAngleDeg();
  float frontRightMovement = -sin((movementAngle - 40.0).toRad().value);
  float frontLeftMovement = sin((movementAngle + 40.0).toRad().value);
  float backRightMovement = -sin((movementAngle + 40.0).toRad().value);
  float backLeftMovement = sin((movementAngle - 40.0).toRad().value);
  // Serial.println("moveeeeeeee " + String(frontRightMovement) +
  //                " move angle: " + String(movementAngle.value));
  frontRightMovement = max(-1.0, min(1.0, frontRightMovement + fixedRotation));
  frontLeftMovement = max(-1.0, min(1.0, frontLeftMovement + fixedRotation));
  backRightMovement = max(-1.0, min(1.0, backRightMovement + fixedRotation));
  backLeftMovement = max(-1.0, min(1.0, backLeftMovement + fixedRotation));

  // Serial.println("this is after the movement calc : " + String(frontRightMovement) + " " + String(frontLeftMovement) + " " + String(backRightMovement) + " " + String(backLeftMovement));

  if (moveVec.isZero()) {
    frontRightMovement = max(-1.0, min(1.0, fixedRotation));
    frontLeftMovement = max(-1.0, min(1.0, fixedRotation));
    backRightMovement = max(-1.0, min(1.0, fixedRotation));
    backLeftMovement = max(-1.0, min(1.0, fixedRotation));
    //Serial.print("Just rotate");
  } else {
    // motor power optimizaiton
    float largetMovement =
        max(max(abs(frontRightMovement), abs(frontLeftMovement)),
            max(abs(backRightMovement), abs(backLeftMovement)));
    frontRightMovement = frontRightMovement / largetMovement;
    frontLeftMovement = frontLeftMovement / largetMovement;
    backRightMovement = backRightMovement / largetMovement;
    backLeftMovement = backLeftMovement / largetMovement;
  }
  if(goalie && !LineProcess::isPointingOut){ //makes scoop work as goalie 
    ProportionOfMaxSpeed = GoalieProcess::mag;
  }else{
    ProportionOfMaxSpeed = 1;
  }
  // Serial.println("THE IMPOSTOR PROPORTION: " + String(ProportionOfMaxSpeed));
  motors.frontRight.setPower(ProportionOfMaxSpeed * power * (max(-1.0, min(1.0, frontRightMovement))) + (1 - ProportionOfMaxSpeed) * power * fixedRotation);
  motors.frontLeft.setPower(ProportionOfMaxSpeed * power * (max(-1.0, min(1.0, frontLeftMovement))) + (1 - ProportionOfMaxSpeed) * power * fixedRotation);
  motors.backRight.setPower(ProportionOfMaxSpeed * power * (max(-1.0, min(1.0, backRightMovement))) + (1 - ProportionOfMaxSpeed) * power * fixedRotation);
  motors.backLeft.setPower(ProportionOfMaxSpeed * power * (max(-1.0, min(1.0, backLeftMovement))) + (1 - ProportionOfMaxSpeed) * power * fixedRotation);
  // Serial.println(
  //   String(motors.frontRight.power) +
  //   " " +
  //   String(motors.frontLeft.power) +
  //   " " +
  //    String(motors.backRight.power) +
  //   " " +
  //   String(motors.backLeft.power)
  // );
}
