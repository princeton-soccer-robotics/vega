#include "DribblerProvider.h"

int DribblerProvider::taskHandle = 0;
Servo DribblerProvider::servo = Servo();

void DribblerProvider::make() {
  VEGA_CREATE_TASK();
	servo.attach(A9);
	servo.writeMicroseconds(1100);
	delay(3000);
}

void DribblerProvider::handler() {
	
}

void DribblerProvider::beginDribble() {
	servo.writeMicroseconds(1900);
}

void DribblerProvider::stopDribble() {
	servo.writeMicroseconds(1100);
}