#pragma once

#include <MCP3XXX.h>
#include <Polaris.h>
#include "Settings.h"
#include <framework/TaskManagement.h>

struct LineSensorProvider {
  static int taskHandle;

  static bool triggered;
  static uint32_t thresholds[LINE_SENSORS];
  static uint32_t pickup_thresholds[LINE_SENSORS];
  static uint32_t readings[LINE_SENSORS];
  static MCP3008 adc;
  static MCP3008 adc2;

  static void make();
  static void handler();
  static void refreshReadings();
  static uint32_t readLineSensor(int x);
};  // struct LineSensorProvider