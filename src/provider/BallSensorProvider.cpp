#include "provider/BallSensorProvider.h"

MCP3008 BallSensorProvider::adc;
MCP3008 BallSensorProvider::adc2;
MCP3008 BallSensorProvider::adc3;
float BallSensorProvider::cosVals[BALL_SENSORS] = {0};
float BallSensorProvider::sinVals[BALL_SENSORS] = {0};
polaris::Vec2 BallSensorProvider::ballPos = polaris::Vec2(0, 0);
float BallSensorProvider::magnitude = 0;

int BallSensorProvider::ballValues[BALL_SENSORS] = {0};

void BallSensorProvider::make() {
  adc.begin(BALL_SPI_CS_PIN_1);
  adc2.begin(BALL_SPI_CS_PIN_2);
  adc3.begin(BALL_SPI_CS_PIN_3);
  float step = 90;
  float increment = 360 / BALL_SENSORS;
  for (int i = 0; i < BALL_SENSORS; i++) {
    cosVals[i] = cos(AngleDeg(step).toRad().value);
    sinVals[i] = sin(AngleDeg(step).toRad().value);
    step -= increment;
    // Serial.println("i: " + String(i) + " cosVal: " + cosVals[i] + " sinVal: "
    // + sinVals[i]);
  }
}

void BallSensorProvider::refresh() {
  
  for (int i = 0; i < BALL_SENSORS; i++) {
    ballValues[i] = 1050 - readBallSensor(i);
  }
  
}

uint32_t BallSensorProvider::readBallSensor(int x) {
  
  uint32_t reading;
  if (x < 8) {
    reading = adc.analogRead(x);
  } else if (x < 16) {
    reading = adc2.analogRead(x - 8);
  } else {
    reading = adc3.analogRead(x - 16);
  }
  
  return reading;
}

tuple<polaris::Vec2, float> BallSensorProvider::getBallPos(bool refreshValues) {
  
  if (refreshValues)
    BallSensorProvider::refresh();
  
  polaris::Vec2 ballPos = polaris::Vec2(0, 0);
  
  // Serial.println(String(ballPos.x) + " " + String(ballPos.y));
  for (int i = 0; i < BALL_SENSORS; i++) {
    if (BallSensorProvider::ballValues[i] < BALL_VALUE_FILTER ||
        BallSensorProvider::ballValues[i] > 1050)
      continue;
  }
  
  int copiedValues[BALL_SENSORS] = {0};
  int indices[BALL_SENSORS] = {0};
  for (int i = 0; i < BALL_SENSORS; i++) {
    copiedValues[i] = BallSensorProvider::ballValues[i];
    indices[i] = i;
  }
  
  polaris::quickSort(copiedValues, indices, 0,
                     sizeof(copiedValues) / sizeof(copiedValues[0]) - 1);
  
  for (int i = BALL_SENSORS - 4; i < BALL_SENSORS; i++) {
    ballPos.x += BallSensorProvider::cosVals[indices[i]] * copiedValues[i];
    ballPos.y += BallSensorProvider::sinVals[indices[i]] * copiedValues[i];
  }
  
  float magnitude = ballPos.magnitude();
  // 
  BallSensorProvider::ballPos = ballPos;
  BallSensorProvider::magnitude = magnitude;
  return make_tuple(ballPos, magnitude / 2600);
}