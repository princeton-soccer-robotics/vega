#pragma once

#include <Polaris.h>
#include <Settings.h>
#include <framework/TaskManagement.h>
#include "process/globals.h"
#include <sstream>
#include <string>

struct CameraProvider {
  static polaris::Vec2 goalVec;
  static std::string buffer;
  
  static int lastSend;
  static int lastFriendlySend;
  static int lastEnemySend;

  static float friendlyDist;
  static float friendlyAngle;
  static float enemyDist;
  static float enemyAngle;
  static bool seesFriendly;
  static int preventCrashes;
  static float selfX;
  static float selfY;


  static void make();
  static void handler();
  static float nextFloat(std::istringstream& split);
};