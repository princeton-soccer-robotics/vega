#pragma once

#include <MCP3XXX.h>
#include <Polaris.h>
#include <XBee.h>
#include <string>
#include <tuple>
#include "Settings.h"

using namespace polaris;

struct BallSensorProvider {
  static MCP3008 adc;
  static MCP3008 adc2;
  static MCP3008 adc3;
  static float cosVals[BALL_SENSORS];
  static float sinVals[BALL_SENSORS];
  static polaris::Vec2 ballPos;
  static float magnitude;

  static int ballValues[BALL_SENSORS];

  static void make();

  static void refresh();

  static uint32_t readBallSensor(int x);

  static tuple<polaris::Vec2, float> getBallPos(bool refreshValues);
};