#pragma once

#include <Settings.h>
#include <framework/TaskManagement.h>
#include "provider/BallSensorProvider.h"

struct LightGateProvider {
  static int taskHandle;

  static bool triggered;

  static void make();

  static void handler();

  static uint32_t readLineSensor(int x);
};  // struct LightGateProvider