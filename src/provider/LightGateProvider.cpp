#include "LightGateProvider.h"

int LightGateProvider::taskHandle = 0;
bool LightGateProvider::triggered = false;

void LightGateProvider::make() {
  VEGA_CREATE_TASK();
}

void LightGateProvider::handler() {
  triggered = analogRead(LIGHT_GATE_PIN) < LIGHT_GATE_THRESHOLD;
}