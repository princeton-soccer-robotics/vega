#pragma once

#include <Settings.h>
#include <framework/TaskManagement.h>
#include <Servo.h>

struct DribblerProvider {
  static int taskHandle;
	static Servo servo;

  static void make();

  static void handler();

	static void beginDribble();

	static void stopDribble();
};  // struct KickerProvider