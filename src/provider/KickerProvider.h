#pragma once

#include <Settings.h>
#include <framework/TaskManagement.h>

struct KickerProvider {
  static int taskHandle;
	static int kickStart;
	static bool kicking;

  static void make();

  static void handler();

	static void kick();
};  // struct KickerProvider