#pragma once

#include <Adafruit_BNO055.h>
#include <Polaris.h>
#include <Settings.h>
#include <framework/TaskManagement.h>
#include <provider/MovementProvider.h>

struct CompassProvider {
  static int taskHandle;

  static Adafruit_BNO055 bno;

  static float rotation;
  static float targetRotation;

  static void make();
  static void setOrigin();
  static void handler();
};