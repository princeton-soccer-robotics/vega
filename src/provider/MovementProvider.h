#pragma once

#include <Polaris.h>
#include <Settings.h>
#include <framework/TaskManagement.h>

#include <unordered_map>
#include <utility>
#include "process/globals.h"
#include "process/challenge/TC1.h"

enum VecPriority { LOW_PRIO, MEDIUM_PRIO, HIGH_PRIO, FULL_PRIO };
enum ProcessID { LINE_PROCESS, CENTERING_PROCESS, FOLLOW_PROCESS, GOALIE_PROCESS, TC1_PROCESS };

#define SET_VEC_PRIORITY(ID, PRIO) \
  MovementProvider::processVecs[ID] = make_pair(PRIO, polaris::Vec2(0, 0))

/**
 * Motor is a helper object that automatically configures and controls
 * individual motors on the pololu MCB it also acts as a safety guard so that we
 * don't burn out maxon motors
 **/
struct Motor {
  int dirPin;
  int powPin;
  int power;

  Motor(int dirPin, int powPin);

  void setPower(int power);
  void write();
};

struct Motors {
  Motor frontRight;
  Motor frontLeft;
  Motor backRight;
  Motor backLeft;
};

struct MovementProvider {
  static int taskHandle;

  static polaris::Vec2 moveVec;
  static polaris::Vec2 projectionVec;
  static unordered_map<int, std::pair<VecPriority, polaris::Vec2>> processVecs;
  static int power;
  static float rotation;
  static Motors motors;
  static float ProportionOfMaxSpeed;

  static void make();
  static void handler();

  static void setVec(ProcessID id, polaris::Vec2 vec);
  static void setPrio(ProcessID id, VecPriority prio);
  static void setPower(int power);
  static void recalculate();
};