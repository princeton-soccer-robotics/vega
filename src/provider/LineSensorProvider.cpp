#include "provider/LineSensorProvider.h"

int LineSensorProvider::taskHandle = 0;
uint32_t LineSensorProvider::readings[LINE_SENSORS] = {0};
uint32_t LineSensorProvider::thresholds[LINE_SENSORS] = {500,500,500,500,500,500,500,500,500,500,500,500,500,500,500,500};
uint32_t LineSensorProvider::pickup_thresholds[LINE_SENSORS] = {150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150};
MCP3008 LineSensorProvider::adc;
MCP3008 LineSensorProvider::adc2;

void LineSensorProvider::make() {
  adc.begin(LINE_SPI_CS_PIN_1);
  adc2.begin(LINE_SPI_CS_PIN_2);
  VEGA_CREATE_TASK();
}

void LineSensorProvider::refreshReadings() {
  for (int i = 0; i < LINE_SENSORS; i++) {
    uint32_t averaged = 0;
    for (int j = 0; j < LINE_AVERAGING_COUNT; j++) {
      auto reading = readLineSensor(i);

      // keep doing calc until you are no longer dum
      int freezePrevention =
          0;  // prevents the robot from freezing if sensors are malfunctioned
      while (reading < LINE_SMALL_FILTER || reading > LINE_BIG_FILTER) {
        reading = readLineSensor(i);
        if (freezePrevention > 2) {
          reading = 0;
          break;
        }
        freezePrevention += 1;
      }

      averaged += reading;
    }
    averaged /= LINE_AVERAGING_COUNT;

    readings[i] = averaged;
    //Serial.print(" " + String(averaged));
  }
  //Serial.println();
}

void LineSensorProvider::handler() {
}

uint32_t LineSensorProvider::readLineSensor(int x) {
  uint32_t reading;
  if (x < 8) {
    reading = adc.analogRead(x);
  } else {
    reading = adc2.analogRead(x - 8);
  }
  return reading;
}
