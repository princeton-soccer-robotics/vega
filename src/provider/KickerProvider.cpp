#include "KickerProvider.h"

int KickerProvider::taskHandle = 0;
int KickerProvider::kickStart = 0;
bool KickerProvider::kicking = false;

void KickerProvider::make() {
  VEGA_CREATE_TASK();
}

void KickerProvider::handler() {
	if (millis() - kickStart > 30) {
		digitalWrite(2,LOW);
		kicking = false;
	}
}

void KickerProvider::kick() {
	if(millis() - kickStart > 5000){
		digitalWrite(2,HIGH);
		kickStart = millis();
		kicking = true; 
	}
}