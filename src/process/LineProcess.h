#pragma once
#include <ArduinoJson.h>
#include <Polaris.h>
#include "Settings.h"

#include "process/offense/FollowProcess.h"
#include "process/offense/GoalProcess.h"
#include "provider/LineSensorProvider.h"
#include "provider/MovementProvider.h"
#include "provider/CompassProvider.h"

using namespace polaris;

struct LineProcess {
  static Vec2 lineVecs[LINE_SENSORS];
  static Vec2 previousVec;
  static Vec2 currentVec;
  static Vec2 LineVecForOtherProcesses;
  static bool isPointingOut;
  static int detections;
  static float compassDirectionHolder;
  static Vec2 CurrentVecForOtherPrograms;

  static void make();

  static void handler();
};