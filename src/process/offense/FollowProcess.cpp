#include "process/offense/FollowProcess.h"
#include <provider/CameraProvider.h>

int FollowProcess::taskHandle = 0;

void FollowProcess::make() {
  VEGA_CREATE_TASK();
  SET_VEC_PRIORITY(ProcessID::FOLLOW_PROCESS, VecPriority::MEDIUM_PRIO);
}

float FollowProcess::dampen(float x) {
  return fmin(1.0, 0.048 * pow(M_E, 3.2 * x));
  // return fmax(0, fmin(1, 0.02 * pow(1.0001, 20 * (x - 10))));
}

float FollowProcess::getBallOffset(float inAngle) {
  float formulaAngle = inAngle > 180.0 ? 360 - inAngle : inAngle;
  return (fmin(0.05 * pow(M_E, 0.15 * formulaAngle + 1.2), 90));
}

float FollowProcess::getOptimalAngle(float inAngle,
                                     float magnitude,
                                     bool doDampen = true) {
  //
  float ballOffset = getBallOffset(inAngle);
  if (doDampen)
    ballOffset *= dampen(magnitude);

  float optimalDirection;
  if (inAngle > 180.0) {
    optimalDirection = inAngle - ballOffset;
  } else {
    optimalDirection = inAngle + ballOffset;
  }
  //
  return optimalDirection;
}

void FollowProcess::handler() {
  if (goalie) {
    MovementProvider::setVec(ProcessID::FOLLOW_PROCESS, Vec2(0, 0));
    return;
  }
  //Serial.println("doing follow process" + String(goalie));
  float magnitude;
  polaris::Vec2 ballPos;

  BallSensorProvider::refresh();

  if (BallSensorProvider::ballValues[0] > 500) {
    DribblerProvider::beginDribble();
  } else {
    DribblerProvider::stopDribble();
  }

  // for(int i = 0; i < BALL_SENSORS; i++) {
  //   Serial.print(String(BallSensorProvider::ballValues[i]) + " ");
  // }
  // Serial.println();

  tie(ballPos, magnitude) = BallSensorProvider::getBallPos(false);
  //Serial.println(ballPos.toAngleDeg().value);

  auto optimalAngle =
      getOptimalAngle(ballPos.toAngleDeg().trueAngle().value, magnitude, true);

  auto optimalVec = AngleDeg(optimalAngle).trueAngle().toVec();
  
  //if ball is picked up
  if(magnitude < 0.2){
    MovementProvider::setVec(ProcessID::FOLLOW_PROCESS, Vec2(0,0));
  }else{
    MovementProvider::setVec(ProcessID::FOLLOW_PROCESS, optimalVec);
  }
  
  //lightgate replacement for now. Super bad replacement. When lightgate works we need to replace this asap
  //Serial.println(String(analogRead(LIGHT_GATE_PIN)) + " is value and threshold is " + String(LIGHT_GATE_THRESHOLD));
  if(LightGateProvider::triggered && BallSensorProvider::ballValues[0] > 500){
     KickerProvider::kick();
  }
  //Serial.println(String(magnitude) + " is mag and enemyDist is: " + String(CameraProvider::enemyDist) + "          " + String(magnitude > 0.95 && CameraProvider::enemyDist < 130 && optimalVec.toAngleDeg().value > 80 && optimalVec.toAngleDeg().value < 100));
}