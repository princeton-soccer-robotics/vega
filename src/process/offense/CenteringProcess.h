#pragma once
#include <math.h>
#include <ArduinoJson.h>
#include "Settings.h"
#include "provider/CameraProvider.h"
#include "provider/CompassProvider.h"
#include "provider/MovementProvider.h"
#include "provider/BallSensorProvider.h"
#include "process/globals.h"

struct CenteringProcess {
  static int taskHandle;

  static void make();

  static void handler();
};  // struct CenteringProcess