#include "process/offense/CenteringProcess.h"

int CenteringProcess::taskHandle = 0;

void CenteringProcess::make() {
  VEGA_CREATE_TASK();
  SET_VEC_PRIORITY(ProcessID::CENTERING_PROCESS, VecPriority::LOW_PRIO);
}

void CenteringProcess::handler() {
  StaticJsonDocument<1024> debugDoc;
  polaris::AngleDeg centered = polaris::AngleDeg(0);
  auto ballPosDeg = BallSensorProvider::ballPos.toAngleDeg();
  // bool hasBall = BallSensorProvider::magnitude > 0.94 && (ballPosDeg > 80 &&
  // ballPosDeg < 100);

      // Serial.println("Centering by compass");
  centered = polaris::AngleDeg(CompassProvider::targetRotation - CompassProvider::rotation);
  if (centered.value < 0) {
    centered += 360;
  }
  // Serial.println(centered.value * CENTER_MULTIPLIER);
  if (centered.value < 180) {
    MovementProvider::rotation = (centered.value * CENTER_MULTIPLIER);
  } else {
    MovementProvider::rotation = -((360 - centered.value) * CENTER_MULTIPLIER);
  }
  debugDoc["proc"] = "centering";
  debugDoc["camera_angle"] = CameraProvider::goalVec.toAngleDeg().value;
  debugDoc["compass_angle"] =
      CompassProvider::targetRotation - CompassProvider::rotation;
  debugDoc["last_send_time"] = millis() - CameraProvider::lastSend;
  // serializeJson(debugDoc, Serial);
  // Serial.println();
}