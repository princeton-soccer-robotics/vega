#pragma once

#include <Polaris.h>
#include <framework/TaskManagement.h>
#include "provider/BallSensorProvider.h"
#include "provider/MovementProvider.h"
#include "provider/KickerProvider.h"
#include "provider/DribblerProvider.h"
#include "provider/LightGateProvider.h"

using namespace polaris;

struct FollowProcess {
  static int taskHandle;

  static float dampen(float x);

  static float getBallOffset(float inAngle);

  static float getOptimalAngle(float inAngle,
                               float magnitude,
                               bool doDampen = true);

  static void make();

  static void handler();
};  // struct FollowProcess