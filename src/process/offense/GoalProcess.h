#pragma once
#include <framework/TaskManagement.h>
#include <math.h>
#include "Settings.h"
#include "process/LineProcess.h"
#include "process/offense/FollowProcess.h"
#include "provider/BallSensorProvider.h"

struct GoalProcess {
  static void make();

  static void handler();
};  // struct GoalProcess