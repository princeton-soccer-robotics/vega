#pragma once
#include <Polaris.h>
#include <framework/TaskManagement.h>
#include "process/offense/FollowProcess.h"
#include "provider/LightGateProvider.h"

struct LightGateProcess {
  static int taskHandle;

  float getOptimalAngle(float inAngle, float magnitude);

  static void make();

  static void handler();
};  // struct LightGateProcess