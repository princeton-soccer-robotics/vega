#include "process/LineProcess.h"
#include "challenge/TC1.h"

Vec2 LineProcess::lineVecs[LINE_SENSORS] = {Vec2(0, 0)};
Vec2 LineProcess::previousVec = Vec2(0, 0);
Vec2 LineProcess::currentVec = Vec2(0, 0);
Vec2 LineProcess::LineVecForOtherProcesses = Vec2(0, 0);
bool LineProcess::isPointingOut = false;
Vec2 LineProcess::CurrentVecForOtherPrograms = Vec2(0,0);
// number of line sensors that are triggered
int LineProcess::detections = 0;
float LineProcess::compassDirectionHolder = 0;

void LineProcess::make() {
  for (int i = 0; i < LINE_SENSORS; i++) {
    lineVecs[i] = AngleDeg(22.5 * i).trueAngle().toVec();
  }
  VEGA_CREATE_TASK();
  SET_VEC_PRIORITY(ProcessID::LINE_PROCESS, VecPriority::FULL_PRIO);
}

void LineProcess::handler() {
  StaticJsonDocument<1024> debugDoc;
  debugDoc["proc"] = "line";
  // get latest readings from line sensor
  LineSensorProvider::refreshReadings();

  float changeInDirection = 0;

  auto lineSensorGroups = 0;

  // flag that says if robot is picked up or not
  int pickupSensors = 0;

  auto isPickedUp = false;

  // flag that says if koig ring should run
  auto shouldRun = false;

  detections = 0;

  // the vector of koig ring triggers
  auto currentVec = Vec2(0, 0);

  auto transformed = Vec2(0, 0);

  bool triggered[LINE_SENSORS] = {0};

  JsonArray sensorVals = debugDoc.createNestedArray("sensor_vals");
  JsonArray triggeredDebug = debugDoc.createNestedArray("triggered");

  for (int i = 0; i < LINE_SENSORS; i++) {
    const auto sensorVal = LineSensorProvider::readings[i];
    sensorVals.add(sensorVal);
    if (sensorVal > LineSensorProvider::thresholds[i]) {
      //Serial.println(" TRIGGEREDDDDD " + String(i));
      triggered[i] = true;
      triggeredDebug[i] = true;
      detections++;
      if (i == LINE_SENSORS - 1) {
        if (!triggered[0] && !triggered[i - 1]) {
          lineSensorGroups++;
        }
      } else if (i == 0) {
        lineSensorGroups++;
      } else if (!triggered[i - 1]) {
        lineSensorGroups++;
      }
    } else if (sensorVal < LineSensorProvider::pickup_thresholds[i]) {
      pickupSensors += 1;
    }
  }
  Serial.println(detections);

  int bestI = 0;
  int bestJ = 0;

  int closestAngle = 0;

  for (int i = 0; i < LINE_SENSORS; i++) {
    if (triggered[i]) {
      for (int j = 0; j < LINE_SENSORS; j++) {
        if (triggered[j]) {
          const float diff = lineVecs[i]
                                 .toAngleDeg()
                                 .angleDiff(lineVecs[j].toAngleDeg())
                                 .value;
          if (abs(180 - diff) < abs(180 - closestAngle)) {
            bestI = i;
            bestJ = j;
            closestAngle = diff;
          }
        }
      }
    }
  }

  if (lineSensorGroups == 3){
    changeInDirection = compassDirectionHolder - (CompassProvider::targetRotation - CompassProvider::rotation);
    currentVec = AngleDeg{previousVec.toAngleDeg().value + changeInDirection}.toVec();
    previousVec = currentVec;
    compassDirectionHolder = (CompassProvider::targetRotation - CompassProvider::rotation);
  }else if (bestI + bestJ != 0){
    currentVec += lineVecs[bestI] + lineVecs[bestJ];
  }

  // picked up if a lot of sensors get triggered
  if (pickupSensors > PICKUP_THRESHOLD)
    isPickedUp = true;

  // run koig ring if line sensors trigger, and if the current vector is not
  // zero (direct opposite line sensors)
  if (detections > 1)
    shouldRun = true;

  // run kog ring if we are out and the koig is triggered before
  if (detections < 2 && isPointingOut)
    shouldRun = true;

  if (detections == 0 && !isPointingOut) {
    previousVec = Vec2(0, 0);
    transformed = Vec2(0, 0);
    LineVecForOtherProcesses = currentVec;
  }

  if (shouldRun) {
    // flip vector to face away from line (koig not determined yet)
    currentVec = currentVec.flipped();

    // set previous vector to the current vector if it's zero to prevent bad
    // math

    if (previousVec.isZero())
      previousVec = currentVec;
    if (currentVec.isZero()) {
      // use compass to correct currentVec
      changeInDirection = compassDirectionHolder - (CompassProvider::targetRotation - CompassProvider::rotation);
      currentVec = AngleDeg{previousVec.toAngleDeg().value + changeInDirection}.toVec();
      previousVec = currentVec;
      compassDirectionHolder = (CompassProvider::targetRotation - CompassProvider::rotation);
    } else {
      // update compassDirectionHolder
      compassDirectionHolder =
          (CompassProvider::targetRotation - CompassProvider::rotation);
    }

    // if koig happened last time, and we're out, set current vector to
    // previous vector
    if (detections < 2 && isPointingOut)
      currentVec = previousVec;

    // difference between previous and current
    const float diff =
        currentVec.toAngleDeg().angleDiff(previousVec.toAngleDeg()).value;

    // koig happened if angle difference is more than 90
    if (abs(diff) > 90) {
      // koig.
      isPointingOut = !isPointingOut;
    }
    debugDoc["pointing_out"] = isPointingOut;
    
    // flip if koig.
    if (isPointingOut) {
      currentVec = currentVec.flipped();
    }
    MovementProvider::projectionVec = currentVec;

    // Serial.println("currentVec: " + currentVec.toString() + " previousVec: " +
    //                previousVec.toString() + " compassDirectionHolder: " +
    //                String(changeInDirection) + " ispointingpout: " + String(isPointingOut) + " lineSensorGroups: " + String(lineSensorGroups));

    if ((isPointingOut || detections > 6) && TC1Process::State != 3 && currentVec.y > -0.5) {
      // Serial.println("It is correcting the line vec with compass. currentVec:
      // " + currentVec.toString() + " compassDirectionHolder: " +
      // String(compassDirectionHolder) + " ");
      // set moveVec to away-from-line vector (koig included)
      MovementProvider::projectionVec = Vec2(0, 0);
      MovementProvider::setVec(ProcessID::LINE_PROCESS, currentVec);
    } else {
      MovementProvider::setVec(ProcessID::LINE_PROCESS, Vec2(0, 0));
    }
    JsonObject koigVec = debugDoc.createNestedObject("koig_vec");
    Vec2 currentUnitVec = currentVec.toUnitVector();
    koigVec["x"] = currentUnitVec.x;
    koigVec["y"] = currentUnitVec.y;
    CurrentVecForOtherPrograms = currentVec;
    // flip back to non-koig if koig happen.
    if (isPointingOut)
      currentVec = currentVec.flipped();
    previousVec = currentVec;
  } else {
    // Serial.println("setting vec to 0");
    CurrentVecForOtherPrograms = currentVec;
    MovementProvider::setVec(ProcessID::LINE_PROCESS, Vec2(0, 0));
    MovementProvider::projectionVec = Vec2(0, 0);
  }
  

  // if (isPickedUp) {
  //   Serial.println("Just got picked up lmao");
  //   MovementProvider::moveVec = Vec2(0, 0);
  //   isPointingOut = false;
  // }
  //serializeJson(debugDoc, Serial);
  //Serial.println();
}
