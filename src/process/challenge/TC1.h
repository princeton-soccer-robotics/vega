#pragma once

#include <Polaris.h>
#include <framework/TaskManagement.h>
#include <Arduino.h>
#include "provider/MovementProvider.h"
#include "provider/BallSensorProvider.h"

using namespace polaris;

struct TC1Process {
  static void make();
  static int State;
  static int rotationoffset;

  static void handler();
  static void GotoPosition1();
  static bool isSwapNow;
};  // struct TC1Process