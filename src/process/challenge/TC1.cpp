#include "TC1.h"
#include "process/offense/FollowProcess.h"
#include "provider/CameraProvider.h"
#include "provider/CompassProvider.h"
#include "provider/DribblerProvider.h"
#include "provider/MovementProvider.h"
#include "process/LineProcess.h"
#include <Polaris.h>

int TC1Process::State;
int TC1Process::rotationoffset;
int timer;
int recoveryPoint;
int starttime;
bool isGiver;
int goBacktimer;
int compass;
bool waitingfordribbler;
bool lightgatealreadytriggered;
int rotationoffset;
int rotationTimer;
void TC1Process::make() {
  VEGA_CREATE_TASK();
  SET_VEC_PRIORITY(ProcessID::TC1_PROCESS, VecPriority::MEDIUM_PRIO);
  State = 2;
  timer = 0;
  isGiver = true;
  compass = 0;
  goBacktimer = 0;
  recoveryPoint = 0;
  starttime = getMS();
  rotationoffset = 0;
  
  //DribblerProvider::beginDribble();
}


void TC1Process::handler() {
  Vec2 ballPos;
  float magnitude;
  BallSensorProvider::refresh();
  tie(ballPos, magnitude) = BallSensorProvider::getBallPos(false);
  
  Serial.println("State: " + String(State) + " currentVec.x: " + String(LineProcess::CurrentVecForOtherPrograms.x) + " isGiver: " + String(isGiver) + "detections: " +String(LineProcess::detections) + " lightgate: " + String(analogRead(LIGHT_GATE_PIN)) + " Line vector: " + String(LineProcess::CurrentVecForOtherPrograms.toUnitVector().toString()));
  compass = AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation}.toRad().value;
  
  float bigBrainCameraAngle = CameraProvider::friendlyAngle + compass;
  //Serial.println( "compass: " + String(compass) + " friend angle:  " + String(CameraProvider::friendlyAngle) + " angle to goal: "+String(bigBrainCameraAngle) + " bigBrainDIstance: " + String(26*tan(bigBrainCameraAngle)));
  if(isGiver){//we are giving
    if(State == 0){//go to position1
    //MovementProvider::ProportionOfMaxSpeed = 1;
      if(LineProcess::CurrentVecForOtherPrograms.x < 0){//we are on the line
        if(getMS() - goBacktimer < 600){
          //Vec2 TargetVector = AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation + 180}.trueAngle().toVec();
          Vec2 TargetVector = Vec2(0, -1); 
          MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);//move back
        }else{//at position 1
          MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));//stop
          State++;//move on to the next state
          rotationoffset = 10;
        }
        
      }else{//not on line
        //go right
        Vec2 TargetVector = Vec2(1,0);//AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation + 90}.trueAngle().toVec();
        MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);
      }

    } else if(State == 1){//at position 1, go to position 2
      if(LineProcess::CurrentVecForOtherPrograms.x > 0){//we are at position 1
        MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));//stop
        State++;//move on to the next state
        
      }else{//not on line, go left
        // if(millis() - rotationTimer > 40 && rotationoffset < 15){
        //   rotationoffset += 1;
        //   rotationTimer = millis();
        // }
        Vec2 TargetVector = AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation - 90 + 8}.trueAngle().toVec();
        MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);
        Serial.println("We are moving: " + TargetVector.toString() + " Compass: " + String(CompassProvider::targetRotation - CompassProvider::rotation));
      }
    } else if(State == 2){//go to exchangepostiion2
    DribblerProvider::stopDribble();
    
      //Serial.println("currentvec Y "+String(LineProcess::CurrentVecForOtherPrograms.toUnitVector().toString()));
      if(LineProcess::CurrentVecForOtherPrograms.x > 0){//we are on the line
      //Serial.println("   " + String(LineProcess::CurrentVecForOtherPrograms.toUnitVector().y));
        if(LineProcess::detections < 10 || LineProcess::CurrentVecForOtherPrograms.toUnitVector().y > -0.5){//we are not on the centerline
          Vec2 TargetVector = AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation}.trueAngle().toVec() + Vec2(-0.2,0);
          MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector.toUnitVector());//move forward
          //MovementProvider::ProportionOfMaxSpeed = 0.6;
        }else{
          //rotationoffset = 10;
          MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));//stop
          State++;//move on to the next state
        }
        
      }else{
        //go left
        Vec2 TargetVector = Vec2(-1,0);//AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation - 90}.trueAngle().toVec();
        MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);
      }
    } else if(State == 3){
      //wait for confirmation from teammate and then turn off dribbler and switch roles
      
      if(analogRead(LIGHT_GATE_PIN) > LIGHT_GATE_THRESHOLD){
        
        isGiver = false;
        State = 0;
        goBacktimer = getMS();
      }
      MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));
      
    } 
  }else{//we are recieving
    if(State == 0){
      rotationoffset = -5;
      //MovementProvider::ProportionOfMaxSpeed = 1;
      //go to position2
      //if(LineProcess::CurrentVecForOtherPrograms.x > 0){//we are on the line
        if(getMS() - goBacktimer < 600){
          //Vec2 TargetVector = AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation + 180}.trueAngle().toVec();
          Vec2 TargetVector = Vec2(0,-1);
          MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);
        
        }else{
          MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));
          State++;//move on to the next state
        }
        
      // }else{
      //   //go left
      //   Vec2 TargetVector = AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation - 90}.trueAngle().toVec();
      //   MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);
      // }


    } else if(State == 1){//go to position 1
      if(LineProcess::CurrentVecForOtherPrograms.x < 0){//we are at position 1
        MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));
        State++;//move on to the next state
        DribblerProvider::beginDribble();
      }
      else{
        //go right
        Vec2 TargetVector = Vec2(1,0);//AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation + 90}.trueAngle().toVec();
        MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);
      }
    } else if(State == 2){
      //go to exchangepostiion1
      if(LineProcess::CurrentVecForOtherPrograms.x < 0){//we are on the line
      DribblerProvider::beginDribble();
        //Serial.println("Unit vector y " + String(LineProcess::CurrentVecForOtherPrograms.toUnitVector().y));
        if(LineProcess::detections < 10 || LineProcess::CurrentVecForOtherPrograms.toUnitVector().y > -0.5 && analogRead(LIGHT_GATE_PIN) > LIGHT_GATE_THRESHOLD){
          Vec2 TargetVector = AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation}.trueAngle().toVec() + Vec2(0.2,0);
          MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);
          //MovementProvider::ProportionOfMaxSpeed = 0.6;
        }else{
          MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));
          
          State++;//move on to the next state
          Serial.println(" We just switched so drivbbbler shoyld have gone on");
        }
        
      }else{
        //go right
        Vec2 TargetVector = AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation + 90 - 1}.trueAngle().toVec();
        MovementProvider::setVec(ProcessID::TC1_PROCESS, TargetVector);
      }
    } else if(State == 3){
      //measure lightgate and then turn on dribbler and switch roles

      
      if(analogRead(LIGHT_GATE_PIN) < LIGHT_GATE_THRESHOLD  && !lightgatealreadytriggered){
        
        lightgatealreadytriggered = true;
        waitingfordribbler = getMS();
      }else{
        if(BallSensorProvider::ballPos.x > 0.1){
            MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(1,0));
        }else if(BallSensorProvider::ballPos.x < -0.1){
          MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(-1,0));
        }else{
          MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));
        }

      }
      if(getMS() - waitingfordribbler > 100  && lightgatealreadytriggered){
        lightgatealreadytriggered = false;

        
        isGiver = true;
        State = 0;
        waitingfordribbler = 0;
        goBacktimer = getMS();

      }
      MovementProvider::setVec(ProcessID::TC1_PROCESS, Vec2(0,0));
    } 
  } 
}