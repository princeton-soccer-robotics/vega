#include "GoalieProcess.h"
#include <Polaris.h>
#include <process/LineProcess.h>
#include <process/offense/CenteringProcess.h>

int GoalieProcess::taskHandle = 0;
float GoalieProcess::mag = 0;

void GoalieProcess::make() {
  VEGA_CREATE_TASK();
  SET_VEC_PRIORITY(ProcessID::GOALIE_PROCESS, VecPriority::MEDIUM_PRIO);
}

void GoalieProcess::handler() {
  if (!goalie) {
    MovementProvider::setVec(ProcessID::GOALIE_PROCESS, Vec2(0, 0));
    return;
  }

  // if (!CameraProvider::seesFriendly) {
  //   mag = 1.0;
  //   // Serial.println("na um uwu");
  //   MovementProvider::setVec(ProcessID::GOALIE_PROCESS,
  //                            AngleDeg{180 + (CompassProvider::targetRotation
  //                            -
  //                                            CompassProvider::rotation)}
  //                                .trueAngle()
  //                                .toVec());
  //   // Serial.println("the vector imma recover to is: " +
  //   //                AngleDeg{180 + (CompassProvider::targetRotation -
  //   //                                CompassProvider::rotation)}
  //   //                    .trueAngle()
  //   //                    .toVec()
  //   //                    .toString());
  //   return;
  // }

  // Serial.println("doing goalie process" + String(goalie));
  float magnitude;
  polaris::Vec2 ballPos;
  polaris::Vec2 added;
  tie(ballPos, magnitude) = BallSensorProvider::getBallPos(true);
  auto Angle_to_goal = AngleDeg{0};

  auto compassVec =
      AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation}
          .trueAngle()
          .toVec()
          .toUnitVector();

  
  Serial.println("Friendly dist = " + String(CameraProvider::friendlyDist));
  if (millis() - CameraProvider::lastFriendlySend <
      CAMERA_TIMEOUT){  // && CameraProvider::friendlyDist < 240) {  // commented out because distance by camera is unreliable
    Angle_to_goal = CameraProvider::goalVec.toAngleDeg().trueAngle();
    // if(polaris::dotProduct(LineProcess::currentVec.toUnitVector(), AngleDeg{Angle_to_goal}.toVec().toUnitVector()) > 0.3){ //if we are on the left most edge or right most edge of the field (NOT goalie box)
    //     Serial.println("we ar eon th eedge");
    //     MovementProvider::setVec(ProcessID::GOALIE_PROCESS, AngleDeg{Angle_to_goal}.toVec().toUnitVector());//go back to goal
    //     mag = 1;
    //     return;
    // }  //we are so farout of the goalie box that we are detecting line on the edge of the field to the right or left)
  } else {  // if we dont see our own goal
    if (polaris::dotProduct(LineProcess::currentVec.toUnitVector(), compassVec) > 0.8) {//if we are back against the line
      MovementProvider::setVec(ProcessID::GOALIE_PROCESS, Vec2(0, 1));//go forward
      Serial.println("Go forward to try to locate goal");
    

    } else {
      Angle_to_goal = (AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation} + 180).trueAngle();  // go back towards our side
      Serial.println(" have a print dot product: " + String(!LineProcess::LineVecForOtherProcesses.isZero()));
      if(magnitude > 0.3 && ballPos.toUnitVector().y < -0.3) {// If ball is directly behind then orbit around
        Serial.println("We be orbiting");
        auto optimalAngle = FollowProcess::getOptimalAngle(
            ballPos.toAngleDeg().trueAngle().value, magnitude, true);
        auto optimalVec = AngleDeg(optimalAngle).trueAngle().toVec();
        MovementProvider::setVec(ProcessID::GOALIE_PROCESS, optimalVec);
      } else {
          Serial.println("We just be going backwards");
        MovementProvider::setVec(
            ProcessID::GOALIE_PROCESS,
            AngleDeg{Angle_to_goal}.toVec().toUnitVector());
      }
      Serial.print(
          String(AngleDeg{Angle_to_goal}.toVec().toUnitVector().toString()));
    }

    mag = 1;
    return;
  }

  
  
  // Serial.println(((90 - abs(AngleDeg{CompassProvider::targetRotation -
  // CompassProvider::rotation}.value))/90.0));
  added = ballPos.toUnitVector() +
          AngleDeg{Angle_to_goal}.toVec().toUnitVector() * 0.7 + (AngleDeg{CompassProvider::targetRotation - CompassProvider::rotation} + 180).trueAngle().toVec().toUnitVector() * 0.2;  // + Vec2(0, 0.1);
  
  if(magnitude < 0.3){ //If we dont see the ball, just act as if the ball is in front (the robot should naturally recover to middle)
    Serial.println("Ok dont see ball and vec to goal is: " + String(AngleDeg{Angle_to_goal}.toVec().toUnitVector().toString()));
    added = compassVec * 0.8 + AngleDeg{Angle_to_goal}.toVec().toUnitVector();
    //added = added * (compassVec.toUnitVector() + trueCamGoalVec.toVec().toUnitVector()).magnitude() * 0.5;//make the autohome less powerful as it gets closer to home
    Serial.println("Dont see ball " + String(added.toString()));
    if(added.magnitude() > 0.8){
      added = (added / added.magnitude()) * 0.8;
    }
  }


  added = added * added.magnitude() * GoalieSensitivity;

  
  
  Serial.println("mmagnitutde:;  " + String(added.magnitude()));
  
  // if (LineProcess::detections != 0) {
  //   added += Vec2(0,.9);
  // }
  // stop moving if on the edge of the goal
  /* if(CompassProvider::targetRotation - CompassProvider::rotation > 30 &&
  CompassProvider::targetRotation - CompassProvider::rotation < 180 &&
  added.x < 0){ added = Vec2(0,0); }else if(CompassProvider::targetRotation
  - CompassProvider::rotation < 330 && CompassProvider::targetRotation -
  CompassProvider::rotation > 180 && added.x > 0){ added = Vec2(0,0);
  } */

  auto trueCamGoalVec = CameraProvider::goalVec.toAngleDeg().trueAngle();


  // if(fabs(polaris::dotProduct(AngleDeg{Angle_to_goal}.toVec().toUnitVector(),
  // LineProcess::previousVec.toUnitVector())) < 0.6){//compare goalVec and
  // LineVec to see if we are out of the goaliebox
  //   MovementProvider::setVec(ProcessID::GOALIE_PROCESS,
  //   AngleDeg{Angle_to_goal}.toVec().toUnitVector() + compassVec);//if not in
  //   goalieBox, then get back towards the goalie box Serial.println("Ïma go
  //   bacc"); return;
  // }

  // if the robot is at the edge of the goalie box and the ball is on the other
  // side making the added vec project the opposite way we want to go, then
  // correct that
  if (magnitude > 0.3 && polaris::dotProduct(AngleDeg{Angle_to_goal}.toVec().toUnitVector(),
                          ballPos.toUnitVector()) > 0.6) {
    Serial.println("Ball is on other side so I gata go around... sigh");
    added += compassVec;
  }

  // auto lineVecUnit = LineProcess::previousVec.toUnitVector();
  // auto product = polaris::dotProduct(compassVec, trueCamGoalVec);
  // Serial.println(compassVec.toString() + " " + trueCamGoalVec.toString() + "
  // " + String(product));

  // compare line and compass to see if robot is at edge of goalie box and then
  // sees if the ball is further out in which case the robot can stop to save
  // energy and not wreck the wheels Serial.println("    " +
  // String(!LineProcess::previousVec.isZero()) + "    " +
  // String(fabs(polaris::dotProduct(LineProcess::previousVec.toUnitVector(),
  // compassVec)) < 0.1) +"    " + String(trueCamGoalVec.toVec().x *
  // ballPos.toUnitVector().x < 0));
  if (!LineProcess::previousVec.isZero() &&
      fabs(polaris::dotProduct(LineProcess::previousVec.toUnitVector(),
                               compassVec)) < 0.1 &&
      trueCamGoalVec.toVec().x * ballPos.toUnitVector().x < 0) {
    Serial.println("I aint getting that");
    added = Vec2(0, 0);
  }

  // compare angle to goal and angle of compass to see if we are on the edge of
  // the goal and stop if ball is out
  // if (fabs(trueCamGoalVec.angleDiff(compassVec.toAngleDeg()).value) < 155) {
  //     // Serial.println("Ïm not moving" +
  //     String(trueCamGoalVec.angleDiff(compassVec.toAngleDeg()).value));
  //     if(fabs((trueCamGoalVec.toVec() +
  //     compassVec).toAngleDeg().angleDiff(added.toAngleDeg()).value) > 90) {
  //       added = Vec2(0,0);
  //     }
  // }

  // if (abs(product) < 0.3 && !lineVecUnit.isZero()) {
  //   if (compassVec.x > 0 && added.x < 0) {
  //     added = Vec2(0, 0);
  //   } else if (compassVec.x < 0 && added.x > 0) {
  //     added = Vec2(0, 0);
  //   }

  // protects against corner to opposite neutral point problem where robot
  // projects added away from idea path because of adding Angle_to_goal
  // if (compassVec.x < 0 && ballPos.toUnitVector().y < 0 &&
  //     ballPos.toUnitVector().x < 0) {
  //   Serial.println("Executing goal_agnel override" +
  //                  ballPos.toUnitVector().toString());
  //   added = ballPos.toUnitVector();
  // } else if (compassVec.x > 0 && ballPos.toUnitVector().y < 0 &&
  //            ballPos.toUnitVector().x > 0) {
  //   Serial.println("Executing goal_agnel override" +
  //                  ballPos.toUnitVector().toString());
  //   added = ballPos.toUnitVector();
  // }
  // } else if (abs(product) < 0.8 && !lineVecUnit.isZero()) {
  //   if (compassVec.x > 0 && added.x < 0) {
  //     added += AngleDeg{Angle_to_goal}.toVec().toUnitVector();
  //     added = added * abs(product) / 2.0;
  //   }
  //   if (compassVec.x < 0 && added.x > 0) {
  //     added += AngleDeg{Angle_to_goal}.toVec().toUnitVector();
  //     added = added * abs(product) / 2.0;
  //   }
  // }
  
  mag = added.magnitude();
  if (mag > 1) {
    added = added / mag;
  }
  if (trueCamGoalVec.toVec().x * ballPos.toUnitVector().x < 0) {
    mag /= 2;
  }

  
  // Serial.println(added.toString() + " " + String(compassAngle) + " " +
  //                AngleDeg{Angle_to_goal}.toVec().toUnitVector().toString()
  //                + " " + String(product) + " " + lineVecUnit.toString() +
  //                " " + String(compassVec.x) + " " + String(added.x));
  // Serial.println("HEWWO UWU YOUR ANGEW TO GO TO IS " + added.toString());
  MovementProvider::setVec(ProcessID::GOALIE_PROCESS, added);
}