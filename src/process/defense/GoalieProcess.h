#pragma once
#include <Polaris.h>
#include <framework/TaskManagement.h>
#include "process/defense/GoalieProcess.h"
#include "provider/BallSensorProvider.h"
#include "provider/CameraProvider.h"
#include "provider/MovementProvider.h"
#include <provider/CompassProvider.h>

struct GoalieProcess {
  static float mag;
  static int taskHandle;

  static void make();

  static void handler();
};  // struct LightGateProcess