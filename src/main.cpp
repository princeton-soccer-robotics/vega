/**
 * The Vega Robot for RoboCup 2019
 * Copyright (C) 2019 Team Orion
 * @authors Keiji, Mark, Niklas, Danil
 **/
#include <EEPROM.h>
#include <Polaris.h>
#include <RamMonitor.h>

#include "Settings.h"

#include "provider/BallSensorProvider.h"
#include "provider/BluetoothProvider.h"
#include "provider/CompassProvider.h"
#include "provider/KickerProvider.h"
#include "provider/LightGateProvider.h"
#include "provider/LineSensorProvider.h"
#include "provider/MovementProvider.h"

#include "process/LineProcess.h"
#include "process/defense/GoalieProcess.h"
#include "process/globals.h"
#include "process/offense/CenteringProcess.h"
#include "process/offense/FollowProcess.h"
#include "process/offense/GoalProcess.h"
#include "process/offense/LightGateProcess.h"
#include "process/challenge/TC1.h"

bool goalie = false;
bool doneCalibrating = false;
int calibrationTime = 0;

void setup() {
  Serial.begin(9600);
  Serial3.begin(9600);

  pinMode(2, OUTPUT);
  digitalWrite(2, LOW);

  pinMode(LINE_SPI_CS_PIN_1, OUTPUT);
  digitalWrite(LINE_SPI_CS_PIN_1, HIGH);
  pinMode(LINE_SPI_CS_PIN_2, OUTPUT);
  digitalWrite(LINE_SPI_CS_PIN_2, HIGH);
  pinMode(BALL_SPI_CS_PIN_1, OUTPUT);
  digitalWrite(BALL_SPI_CS_PIN_1, HIGH);
  pinMode(BALL_SPI_CS_PIN_2, OUTPUT);
  digitalWrite(BALL_SPI_CS_PIN_2, HIGH);
  pinMode(BALL_SPI_CS_PIN_3, OUTPUT);
  digitalWrite(BALL_SPI_CS_PIN_3, HIGH);

  pinMode(SOFTWARE_SWITCH_PIN,
          INPUT_PULLDOWN);  // Electrical forgot to put pulldown on switch so
                            // software fix
  pinMode(CALIBRATION_SWITCH_PIN,
          INPUT_PULLDOWN);  // Electrical forgot to put pulldown on switch so
                            // software fix
  pinMode(RESET_SWITCH_PIN, INPUT_PULLDOWN);
  pinMode(19, INPUT_PULLUP);
  pinMode(18, INPUT_PULLUP);

  MovementProvider::setPower(MOVEMENT_POWER);

#ifdef ENABLE_LINE_SENSOR_PROVIDER
  LineSensorProvider::make();
#endif
#ifdef ENABLE_BALL_SENSOR_PROVIDER
  BallSensorProvider::make();
#endif
#ifdef ENABLE_BLUETOOTH_PROVIDER
  BluetoothProvider::make();
#endif
#ifdef ENABLE_COMPASS_PROVIDER_PROVIDER
  CompassProvider::make();
#endif
#ifdef ENABLE_LIGHT_GATE_PROVIDER
  LightGateProvider::make();
#endif
#ifdef ENABLE_MOVEMENT_PROVIDER
  MovementProvider::make();
#endif
#ifdef ENABLE_CAMERA_PROVIDER
  CameraProvider::make();
#endif
#ifdef ENABLE_KICKER_PROVIDER
  KickerProvider::make();
#endif
#ifdef ENABLE_DRIBBLER_PROVIDER
  DribblerProvider::make();
#endif
#ifdef ENABLE_LINE_PROCESS
  LineProcess::make();
#endif
#ifdef ENABLE_CENTERING_PROCESS
  CenteringProcess::make();
#endif
#ifdef ENABLE_FOLLOW_PROCESS
  FollowProcess::make();
#endif
#ifdef ENABLE_GOAL_PROCESS
  GoalProcess::make();
#endif
#ifdef ENABLE_LIGHT_GATE_PROCESS
  LightGateProcess::make();
#endif
#ifdef ENABLE_GOALIE_PROCESS
  GoalieProcess::make();
#endif
#ifdef ENABLE_TC1
  TC1Process::make();
#endif

  mainController.add(new Thread([&]() {
    if (analogRead(SOFTWARE_SWITCH_PIN) < 600) {
      // OFF
      DribblerProvider::stopDribble();
      MovementProvider::motors.frontRight.setPower(0);
      MovementProvider::motors.frontLeft.setPower(0);
      MovementProvider::motors.backRight.setPower(0);
      MovementProvider::motors.backLeft.setPower(0);
      MovementProvider::motors.frontRight.write();
      MovementProvider::motors.frontLeft.write();
      MovementProvider::motors.backRight.write();
      MovementProvider::motors.backLeft.write();
      while (analogRead(SOFTWARE_SWITCH_PIN) < 600) {
        digitalWrite(2, LOW);  // turn kicker off
        delay(100);
        Serial.println("!!! software switch is off !!!");

        //checks and executes Line calibration (white line threshold and pickup thresholds are calculated with max and min values respectively)
        if (analogRead(CALIBRATION_SWITCH_PIN) > 600) {
          Serial.println("started line calibration");

          //initialize temporary lists to keep track of the most extreem values on the green/black field
          uint32_t maxReadings[LINE_SENSORS] = {0};
          uint32_t minReadings[LINE_SENSORS];
          for(int i = 0; i < LINE_SENSORS; i++){
            minReadings[i] = 700;
          }
          
          //continue to scan for the extreem values while calibration switch is on
          while (analogRead(CALIBRATION_SWITCH_PIN) > 600) {
            LineSensorProvider::refreshReadings();
            for (int i = 0; i < LINE_SENSORS; i++) {
              if (LineSensorProvider::readings[i] > maxReadings[i])
                maxReadings[i] = LineSensorProvider::readings[i];
              if (LineSensorProvider::readings[i] < minReadings[i])
                minReadings[i] = LineSensorProvider::readings[i];
            }    
          }

          //set the white line and pickup thresholds to the maximum and minimum values respectively with a little margin for error (+-10) each sensor had during the calibration
          for (int i = 0; i < LINE_SENSORS; i++) {
            LineSensorProvider::thresholds[i] = maxReadings[i] + 100;
          }
          for (int i = 0; i < LINE_SENSORS; i++) {
            LineSensorProvider::pickup_thresholds[i] = minReadings[i] - 60;
          }

          //THESE PRINT STATEMENTS PRINT THE CALIBRATED 
          // Serial.println("Here are the line thresholds");
          // for (int i = 0; i < LINE_SENSORS; i++) {
          //   Serial.print(String(LineSensorProvider::thresholds[i]) + " ");
          // }
          // Serial.println();
          // Serial.println("Here are the pickup thresholds");
          // for (int i = 0; i < LINE_SENSORS; i++) {
          //   Serial.print(String(LineSensorProvider::pickup_thresholds[i]) + " ");
          // }
          // Serial.println();
        }
      }
      CompassProvider::setOrigin();
      DribblerProvider::servo.write(0);
      doneCalibrating = true;
      calibrationTime = millis();
    }
  }));
}

int lastMeasure = millis();
int ticks = 0;
void loop() {
  mainController.run();
  ticks++;
  if (millis() - lastMeasure > 1000) {
    Serial.println(String(ticks) + " TPS    " + String(CameraProvider::selfX) + ", " + String(CameraProvider::selfY));
    lastMeasure = millis();
    ticks = 0;
    TC1Process::handler();
  }
}