/* Generated by the protocol buffer compiler.  DO NOT EDIT! */
/* Generated from: intercom/intercom.proto */

#ifndef PROTOBUF_C_intercom_2fintercom_2eproto__INCLUDED
#define PROTOBUF_C_intercom_2fintercom_2eproto__INCLUDED

#include <protobuf-c/protobuf-c.h>

PROTOBUF_C__BEGIN_DECLS

#if PROTOBUF_C_VERSION_NUMBER < 1003000
# error This file was generated by a newer version of protoc-c which is incompatible with your libprotobuf-c headers. Please update your headers.
#elif 1003003 < PROTOBUF_C_MIN_COMPILER_VERSION
# error This file was generated by an older version of protoc-c which is incompatible with your libprotobuf-c headers. Please regenerate this file with a newer version of protoc-c.
#endif


typedef struct _EchoRequest EchoRequest;


/* --- enums --- */


/* --- messages --- */

struct  _EchoRequest
{
  ProtobufCMessage base;
  char *data;
};
#define ECHO_REQUEST__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&echo_request__descriptor) \
    , (char *)protobuf_c_empty_string }


/* EchoRequest methods */
void   echo_request__init
                     (EchoRequest         *message);
size_t echo_request__get_packed_size
                     (const EchoRequest   *message);
size_t echo_request__pack
                     (const EchoRequest   *message,
                      uint8_t             *out);
size_t echo_request__pack_to_buffer
                     (const EchoRequest   *message,
                      ProtobufCBuffer     *buffer);
EchoRequest *
       echo_request__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   echo_request__free_unpacked
                     (EchoRequest *message,
                      ProtobufCAllocator *allocator);
/* --- per-message closures --- */

typedef void (*EchoRequest_Closure)
                 (const EchoRequest *message,
                  void *closure_data);

/* --- services --- */


/* --- descriptors --- */

extern const ProtobufCMessageDescriptor echo_request__descriptor;

PROTOBUF_C__END_DECLS


#endif  /* PROTOBUF_C_intercom_2fintercom_2eproto__INCLUDED */
