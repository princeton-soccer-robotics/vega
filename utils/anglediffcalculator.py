import math

def to_angle(x, y):
  return math.degrees(math.atan2(y, x))

def angle_diff(a1, a2):
  if a1 < 0:
    a1 += 360.0
  if a2 < 0:
    a2 += 360.0
  diff = math.fmod((a2 - a1 + 180), 360) - 180
  return diff + 360 if diff < -180 else diff

lines = []
while True:
  line = input()
  if line:
    if not line.startswith("line sensors: ") and not line.startswith("FLIPPERINO "):
      lines.append(line)
  else:
    break
angles = []

for line in lines:
  vec_str = line[2:-2]
  elements = vec_str.replace(", ", " ").split(" ")
  angles.append(to_angle(float(elements[0]), float(elements[1])))

for i in range(len(angles)):
  if i != 0:
    print(angle_diff(angles[i], angles[i-1]))