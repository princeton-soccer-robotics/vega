import argparse
import ballsensorgraph
import ballsensorbestvals
import pygame

graphs = {
  "ball": ballsensorgraph.run,
  "bestvals": ballsensorbestvals.run
}

parser = argparse.ArgumentParser(description='Epic Monitor')
parser.add_argument('--com_port', dest='com_port', help='COM port to monitor on', default='COM8')
parser.add_argument('--graph_type', dest='graph_type', help='graph type (ball,line)', default='ball')

args = parser.parse_args()

if args.graph_type:
  pygame.init()
  pygame.font.init()
  graphs[args.graph_type](args.com_port)