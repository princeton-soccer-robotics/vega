import serial
import pygame
import math

WIDTH = 500
HEIGHT = 500
CENTER = (WIDTH / 2, HEIGHT / 2)

multiplier = [1.038961039, 0.9478672986, 0.8888888889, 0.9501187648, 0.8849557522, 0.8048289738, 0.9259259259, 0.9708737864, 1.133144476, 0.9803921569, 1.408450704, 1.063829787, 0.956937799, 0.9456264775, 0.9661835749, 1.126760563, 0.9592326139, 0.97799511, 1.017811705, 1.055408971, 0.9367681499, 0.8791208791, 0.9592326139, 1.023017903]

def run(com_port):
  s = serial.Serial(com_port, 9600)

  screen = pygame.display.set_mode([WIDTH, HEIGHT])
  text = pygame.font.SysFont('Comic Sans MS', 16)
  running = True
  while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = false
    screen.fill((255, 255, 255))
    raw = s.readline()
    line = str(raw, "ascii")
    print(line)
    if line.startswith("follow_process_debug "):
      line = line.replace("follow_process_debug ", "").strip()
      fields = line.split("|")
      readings = list(map(float, fields[0].strip().split(" ")))
      bestvals = list(map(float, fields[1].strip().split(" ")))
      # for i in range(len(readings)):
      #   readings[i] = int(readings[i] * multiplier[i])
      if len(readings) < 24:
        continue
      for i in range(len(readings)):
        x = math.cos(2 * math.pi / 24 * (i - 6)) * 200
        y = math.sin(2 * math.pi / 24 * (i - 6)) * 200
        pygame.draw.circle(screen, (int((readings[i] / 1100) * 255), 0, 0), (int(x + 250), int(y + 250)), 10)
        screen.blit(text.render(str(readings[i]), False, (0, 0, 0)), (int(x + 230), int(y + 260)))
      for i in range(len(bestvals)):
        x = math.cos(2 * math.pi / 24 * (bestvals[i] - 6)) * 200
        y = math.sin(2 * math.pi / 24 * (bestvals[i] - 6)) * 200
        pygame.draw.circle(screen, (255, 255, 0), (int(x + 250), int(y + 250)), 10)
      pygame.display.update()
  pygame.quit()