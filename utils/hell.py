import serial
import pygame
import math

WIDTH = 500
HEIGHT = 500

CENTER = (WIDTH / 2, HEIGHT / 2)

pygame.init()

screen = pygame.display.set_mode([WIDTH, HEIGHT])

ser = serial.Serial("COM8", 9600)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    screen.fill((255, 255, 255))
    raw = ser.readline()
    line = str(raw, "ascii")
    print(line)
    if line.startswith("line_sensor_readings"):
        strReadings, strBestReadings, strVec = (
            line.replace("line_sensor_readings", "").strip().split(" | ")
        )
        strReadings = strReadings.split(" ")
        strBestReadings = strBestReadings.split(" ")
        strVec = strVec.split(" ")
        readings = list(map(int, strReadings))
        bestI, bestJ = list(map(int, strBestReadings))
        vecX, vecY = list(map(float, strVec))
        pygame.draw.circle(screen, (0, 0, 0), (250, 250), 200, 2)
        for i in range(16):
            x = math.cos(2 * math.pi / 16 * (i - 4)) * 200
            y = math.sin(2 * math.pi / 16 * (i - 4)) * 200
            pygame.draw.circle(screen, (0, 0, 0), (int(x + 250), int(y + 250)), 10)
            if readings[i] > 500:
                pygame.draw.circle(
                    screen, (110, 105, 209), (int(x + 250), int(y + 250)), 10
                )
            if i == bestI or i == bestJ and bestI != -1 and bestJ != -1:
                pygame.draw.circle(
                    screen, (164, 209, 105), (int(x + 250), int(y + 250)), 10
                )
        pygame.draw.line(
            screen, (0, 0, 0), CENTER, (CENTER[0] + 60 * vecX, CENTER[1] - 60 * vecY)
        )
        pygame.display.update()

pygame.quit()