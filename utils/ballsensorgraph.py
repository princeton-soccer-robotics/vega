import serial
import pygame
import math

WIDTH = 500
HEIGHT = 500
CENTER = (WIDTH / 2, HEIGHT / 2)

def run(com_port):
  s = serial.Serial(com_port, 9600)

  screen = pygame.display.set_mode([WIDTH, HEIGHT])
  text = pygame.font.SysFont('Comic Sans MS', 16)
  running = True
  while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = false
    screen.fill((255, 255, 255))
    raw = s.readline()
    line = str(raw, "ascii")
    print(line)
    if line.startswith("follow_process_debug "):
      line = line.replace("follow_process_debug ", "").strip()
      fields = line.split("|")
      readings = list(map(float, fields[0].strip().split(" ")))
      ballPos = list(map(float, fields[1].strip().split(" ")))
      optimalVec = list(map(float, fields[2].strip().split(" ")))
      optimalVecWithoutDampen = list(map(float, fields[3].strip().split(" ")))
      magnitude = int(fields[4].strip())
      if len(readings) < 24:
        continue
      for i in range(len(readings)):
        x = math.cos(2 * math.pi / 24 * (i - 6)) * 200
        y = math.sin(2 * math.pi / 24 * (i - 6)) * 200
        pygame.draw.circle(screen, (int((readings[i] / 1100) * 255), 0, 0), (int(x + 250), int(y + 250)), 10)
        screen.blit(text.render(str(readings[i]), False, (0, 0, 0)), (int(x + 230), int(y + 260)))
        pygame.draw.line(
            screen, (0, 0, 0), CENTER, (CENTER[0] + 120 * ballPos[0], CENTER[1] - 120 * ballPos[1])
        )
        pygame.draw.line(
            screen, (255, 30, 51), CENTER, (CENTER[0] + 120 * optimalVec[0], CENTER[1] - 120 * optimalVec[1])
        )
        pygame.draw.line(
            screen, (30, 255, 255), CENTER, (CENTER[0] + 120 * optimalVecWithoutDampen[0], CENTER[1] - 120 * optimalVecWithoutDampen[1])
        )
        screen.blit(text.render(str(magnitude), False, (0, 0, 0)), (0, 0))
      pygame.display.update()
  pygame.quit()