# how to git

## by Danil Korennykh

### Useful Terms

---

**Remote** - the repository where files get in the cloud

### Essential Commands

---

- Git Pull  
   **Side**:  
  `remote->local`  
  **Description**:  
  `Gets all commits from the remote. Merges all commits into your LOCAL repository. Edits to the same lines of code will result in a merge conflict`

- Git Push  
  **Side**:  
  `local->remote`  
  **Description**:  
  `Pushes all pending commits to remote. Note that if your commits are not in sync with remote, then it will be rejected. You MUST pull first.`
- Git Add  
  **Side**:  
  `local`  
  **Description**:  
  `Adds all files in the directory to the edit queue. Without this, only files already added to git will be tracked.`

- Git Commit  
  **Side**:  
  `local`  
  **Description**:  
  `Saves updates in all added files to a commit (essentially a save point)`

- Git Reset  
  **Side**:  
  `local`  
  **Description**:  
  `Resets local to a specific commit. The commit can be a hash, obtainable using git log, or relative to the latest commit. HEAD resets to the latest commit (deletes all uncommited files), HEAD~1 resets back 1 commit, and HEAD~2 resets back 2 commits.`

### The general steps for putting something on Git

1. `git pull`

2. `git add .`

3. `git commit -m "MESSAGE"`

4. `git push`