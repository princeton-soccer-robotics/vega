#!/usr/bin/env python3

import os
import distutils
import re
import subprocess
from distutils import dir_util

source = os.path.abspath("./protobuf")
tmp = os.path.abspath("./protobuf-build-tmp")
destination = os.path.abspath("./src/protocol")

if os.path.exists(destination):
    distutils.dir_util.remove_tree(destination)
os.mkdir(destination)

# preprocessor for protobuf files in case we need to do stuff with it
def process_proto(path):
    return


distutils.dir_util.copy_tree(source, tmp)

for (dirpath, dirnames, filenames) in os.walk(tmp):
    for f in filenames:
        if f.endswith(".proto"):
            process_proto(os.path.join(dirpath, f))

for (dirpath, dirnames, filenames) in os.walk(tmp):
    for f in filenames:
        if f.endswith(".proto"):
            subprocess.run(
                [
                    "protoc",
                    f"--c_out={destination}",
                    f"--proto_path={tmp}",
                    os.path.join(dirpath, f),
                ]
            )

distutils.dir_util.remove_tree(tmp)
