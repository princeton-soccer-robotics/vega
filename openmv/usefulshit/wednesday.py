import rcjutils, sensor, time, math
from rcjutils import BlobCorners, BlobXYWH

camutils = rcjutils.utils()
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)
clock = time.clock()
camutils.set_camera_adjust(False)
img = sensor.snapshot()
wide = img.width()
high = img.height()
enemyColor = (62, 98, -22, 12, 31, 81)
friendColor = (20, 64, -13, 60, -108, -29)
dangle = 0.5

friend = img.find_blobs(
    [friendColor], pixels_threshold=200, area_threshold=200, merge=True)
enemy = img.find_blobs(
    [enemyColor], pixels_threshold=200, area_threshold=200, merge=True)

enemyDist = 10
friendDist = 10
prevEnemyDist = 10
prevFriendDist = 10

while True:
    img = sensor.snapshot().median(1, percentile=0.5).gamma_corr(gamma = 0.5, contrast = 1.5, brightness = 0)#.crop(roi=[75, 43, 208, 200])

    clock.tick()
    friend = img.find_blobs(
        [friendColor], pixels_threshold=200, area_threshold=200, merge=True)
    enemy = img.find_blobs(
        [enemyColor], pixels_threshold=200, area_threshold=200, merge=True)
    prevFriendDeg = 69420

    friendsCorners = []
    enemiesCorners = []

    for blob in enemy + friend:
        img.draw_edges(blob.min_corners(), color=(255,0,0))
        img.draw_line(blob.major_axis_line(), color=(0,255,0))
        img.draw_line(blob.minor_axis_line(), color=(0,0,255))
    if friend:
        for i in friend:
            friendsCorners.append(BlobCorners.from_oblob(i))
        sorted(friendsCorners, key=BlobCorners.perimeter, reverse=True)
        friendGoal = BlobCorners.from_oblob(friend[0])
        friendGoal.draw_corners(img)
        friendCenter = (friendGoal.center()[0]-59, friendGoal.center()[1]-45)
        friendDeg = rcjutils.VPt.from_list(friendCenter).deg + math.pi
        friendDist = 0.1 * 11370 * math.pow(friend[0].perimeter(), -1.1) + 0.9*prevFriendDist
        prevFriendDeg = friendDeg
        prevFriendDist = friendDist
        print(friendDeg)
    else:
        friendDeg = 69420
        friendDist = 69420
    if enemy:
        for i in enemy:
            enemiesCorners.append(BlobCorners.from_oblob(i))
        sorted(enemiesCorners, key=BlobCorners.perimeter, reverse=True)
        enemyGoal = BlobCorners.from_oblob(enemy[0])
        enemyGoal.draw_corners(img)
        enemyCenter = (enemyGoal.center()[0]-round(img.width()/2), enemyGoal.center()[1]-round(img.height()/2))
        enemyDeg = rcjutils.VPt.from_list(enemyCenter).deg + math.pi
        enemyDist = 0.3 * 11370 * math.pow(enemy[0].perimeter(), -1.1) + 0.7*prevEnemyDist
        if friend and enemy:
            print("Distance: " + str(enemyDist) + " " + str(friendDist)  + " Angle: " + str(enemyDeg)+ " " + str(friendDeg))
            print(str(enemy[0].perimeter()) + " " + str(friend[0].perimeter()))
        prevEnemyDeg = enemyDeg
        prevEnemyDist = enemyDist
    else:
        enemyDeg = 69420
        enemyDist = 69420

        ## goalie enemy
        #goalieangles  = []
        #if len(enemy) > 1:
            #goalieangles  = []
            #for angle in range(enemyGoal.angleRange()/dangle):
                #for goalseg in enemy:
                    #if any(goalseg.isBetween(dangle * angle)):
                        #goalieangles.append(angle * dangle)
        #else:
            #goalieangles.append(69420)
    camutils.send_to_teensy(friendDeg, friendDist, enemyDeg, enemyDist)
    #print(friendDist);
    #print(friendDeg); #format out: angle, distance, angle, distance
