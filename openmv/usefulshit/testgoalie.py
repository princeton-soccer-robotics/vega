import sensor, image, time, sys, math, rcjutils
from pyb import UART
from math import fmod, pi, atan2, floor, tan, cos

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)
clock = time.clock()
camutils = rcjutils.utils()


camutils.setCameraAdjust(False)

sensor.set_auto_gain(False)  # must be turned off for color tracking
sensor.set_auto_whitebal(False)  # must be turned off for color tracking

thresholds = ((33, 100), (-22, 20), (48, 70))
"""
MAIN LOOP
"""

img = sensor.snapshot()
wide = img.width()
high = img.height()

while True:
    img = sensor.snapshot()
    clock.tick()

    """
    COLOR ONE
    """
    b1s = img.find_blobs(
        (thresholds), pixels_threshold=200, area_threshold=200, merge=True
    )
    totalb1 = Blob_CORNERS.from_xywh(blob_XYWH.from_list(blobdown(b1s)))
    for blob in b1s:
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255, 0, 0))
            img.draw_line(blob.major_axis_line(), color=(0, 255, 0))
            img.draw_line(blob.minor_axis_line(), color=(0, 0, 255))
            camutils.drawshit(blob)
        print(totalb1)
    if b1s:
        totalb1 = rcjutils.Blob_CORNERS.from_xywh(rcjutils.blob_XYWH.from_list(blobdown(b1s)))
        for blob in b1s:
            if blob.elongation() > 0.5:
                img.draw_edges(blob.min_corners(), color=(255, 0, 0))
                img.draw_line(blob.major_axis_line(), color=(0, 255, 0))
                img.draw_line(blob.minor_axis_line(), color=(0, 0, 255))
                camutils.drawshit(blob)
            print(totalb1)
            goaliemask = image.Image(img.width(), img.height(), sensor.BINARY)
            totalb1.draw_CORNERS(img)
            potgoalies = img.find_blobs(
                thresholds, roi=camutils.blobdown(b1s), invert=True, merge=False
            )
            candgoalies = rcjutils.Blob_CORNERS.from_xywh(rcjutils.blob_XYWH.from_list(potgoalies))
            candgoalies.draw(img)
