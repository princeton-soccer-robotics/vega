import sensor, image, time, sys, math

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time = 2000)
clock = time.clock()

'''
PREVENTS CAMERA FROM AIUTOMATICALLY CORRECTING IMAGE QUALITIES, PRESERVING LIGHTING
'''
def setCameraAdjust(value):
    sensor.set_auto_exposure(value)
    sensor.set_auto_whitebal(value)
    sensor.set_auto_gain(value)

sensor.setCameraAdjust(False)


'''
VARIOUS CONSTANTS
'''
GAM_COR = 0.8 # GAMMA CORRECTION, INCREASES DARK/LIGHT DIFFERENTIATION
CONTR_COR = 0.9 # CONTRAST CORRECTION, INCREASES COLOR DIFFERENTIATION
BRIGHT_COR = 0.1 # BRIGHTNESS CORRECTION, CAN IMPROVE BAD LIGHTING
A_ONE = (30, 0, 260, 80) # UNUSED & DEPRECATED
allowdiff = [10] * 4


'''
THRESHOLDS FOR BLOB FINDER TO USE
'''
thresholds = [(30, 100, -20, 10, 40, 128)]
            #((0, 100), (-128, 71), (-128, -28))] # generic_blue_thresholds


'''
CONVERTS RECTANGLE TO LIST FOR ITERABILITY
'''
def tolist(rectin):
    return(rectin.x(), rectin.y(), rectin.w(), rectin.h(), rectin.

def isSame(blob1, blob2):
    if blob1 and blob2:
        return all(abs(b1 - b2) < ad for b1, b2, ad in zip(tolist(blob1), tolist(blob2), allowdiff))
    # verify that one blob is similar to the other

def contains(ob, obs):
    if ob and obs:
        return any(isSame(o, ob) for o in obs)
    # verify that list obs contains an object like ob
    # in this case, check that an image contains a blob

def matches(ob, obss):
    if ob and obss:
        return all(contains(ob, obs) for obs in obss)
    # verify that each frame in obss contains an object like ob

'''
DECLARES IMG TYPE FOR USE IN FUNCTIONS
'''

img = sensor.snapshot()


'''
PLACEHOLDER FUNCTION (CURRENTLY UNUSED), APPLIES PREPROCESSING IMAGE FILTERS
'''
def correct(img):
    img.crop(roi=[22, 0, 276, 240])
    img.lens_corr(strength = 8, zoom = 0.55, xcorr=30, ycorr=40).gamma_corr(gamma = GAM_COR, contrast = CONTR_COR, brightness = BRIGHT_COR)# .cartoon(seed_threshold=0.6, floating_thresholds=0.6)
    return img


'''
DEPRECATED PLACEHOLDER FUNCTION, POST-PROCESSES RECOGNIZED BLOBS
'''
def evalrect(rectin, color = "either"):
    imgur = img.get_statistics(roi = tolist(rectin))
    if imgur.l_mean() < -80 and imgur.b_mean() < 64 and (color == 'blue' or color == 'either'):
        return True
    elif imgur.l_mean() > 64 and -20 > imgur.a_mean() > 32 and imgur.b_mean() > 64 and (color == 'yellow' or color == 'either'):
        return True
    else:
        return False

def biggest(listin, spot):
    bigger = 0
    for i in listin:
        if i[spot] > bigger:
            bigger = i[spot]

def smaller(listin, spot):
    smaller = 128
    for i in listin:
        if i[spot] < bigger:
            bigger = i[spot]


'''
DRAWS LABELS FOR BLOBS
'''
def drawshit(r):
    global img
    img.draw_line(r.major_axis_line(), color=(0,255,0))
    img.draw_line(r.minor_axis_line(), color=(0,0,255))
    img.draw_rectangle(r.rect())
    img.draw_cross(r.cx(), r.cy())


done = None # no rect found yet
pastrects = [] # declares two empty lists
goodrects = pastrects

while(not done):
    clock.tick()
    img = correct(sensor.snapshot())
    # pastrects is a list of frames, each of which contains a list of rectangles, each of which has xywh
    # threshold` below should be set to a high enough value to filter out noise
    # rectangles detected in the image which have low edge magnitudes. Rectangles
    # have larger edge magnitudes the larger and more contrasty they are...
    if len(pastrects)>10: # trim off the oldest image, keeping the list at len 10
        pastrects = pastrects[1:]
    rs = img.find_blobs(thresholds, pixels_threshold=200, area_threshold=200, merge=True)
    img.draw_rectangle(0, 0, 320, 120, color=(255,0,0))
    if rs:
    # img.draw_rectangle(imgur.x(), imgur.y(), imgur.w(), imgur.h(), color = image.lab_to_rgb((imgur.l_mean(), imgur.a_mean(), imgur.b_mean())), thickness = 3)
        for r in rs: # for each rectangle
            stats = img.get_statistics(roi=r.rect())
            img.draw_rectangle(r.rect(), color=((image.lab_to_rgb(stats.l_mean(), stats.a_mean(), stats.b_mean()))), fill = True)
            drawshit(r)
            img.draw_keypoints([(r.cx(), r.cy(), int(math.degrees(r.rotation())))], size=20)
            if matches(r, pastrects): # and evalrect(r):
                img.draw_rectangle(r.rect(), color = (255, 0, 0), thickness = 5)
                goodrects.append((r.rect(), (image.lab_to_rgb(stats.l_mean(), stats.a_mean(), stats.b_mean())))) # breaks out of the loop, declares r as a "good" goal
                done = 1
                colorgoals = []
                for i in rs:
                    for j in rs[i]:
                        if isSame(rs[i][j], r):
                            colorgoals.append(rs[i][j])



    pastrects.append(rs) # add latest image to list

img = correct(sensor.snapshot(()))
b1s = img.find_blobs(thresholds[0], pxels_threshold=200, area_threshold=200, merge=True)
b2s = img.find_blobs(thresholds[1], pxels_threshold=200, area_threshold=200, merge=True)
for blob in b1s:
    b1y += blob.cy()
for blob in b2s:
    b2y += blob.cy()

if b1y/len(b1s) > b2y/len(b2s)
    enemy = 1
else:
    enemy = 2


'''
MAIN LOOP
'''

print("*bops* color 1 is done!")
while(True):
    img = correct(sensor.snapshot(()))
    clock.tick()

    '''
    COLOR ONE
    '''
    b1s = img.find_blobs(thresholds[0], pixels_threshold=200, area_threshold=200, merge=True)
    for blob in b1s:
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255,0,0))
            img.draw_line(blob.major_axis_line(), color=(0,255,0))
            img.draw_line(blob.minor_axis_line(), color=(0,0,255))
        img.drawshit(blob)
    for blob in img.find_blobs(thresholds[0], roi=(b1s[1].rect()) pixels_threshold=200, area_threshold=200, invert=True, merge=True):
        pass

'''
COLOR TWO
'''


    for blob in img.find_blobs(thresholds[1], pixels_threshold=200, area_threshold=200, merge=True):
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255,0,0))
            img.draw_line(blob.major_axis_line(), color=(0,255,0))
            img.draw_line(blob.minor_axis_line(), color=(0,0,255))
        img.drawshit(blob)
