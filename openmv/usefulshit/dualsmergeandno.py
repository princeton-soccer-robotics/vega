# Multi Color Blob Tracking Example
#
# This example shows off multi color blob tracking using the OpenMV Cam.

import sensor, image, time, math, framebuf

# Color Tracking Thresholds (L Min, L Max, A Min, A Max, B Min, B Max)
# The below thresholds track in general red/green things. You may wish to tune them...
thresholds = [
    (30, 100, 15, 127, 15, 127),  # generic_red_thresholds
    (30, 100, -64, -8, -32, 32),  # generic_green_thresholds
    (0, 15, 0, 40, -80, -20),
]  # generic_blue_thresholds
# You may pass up to 16 thresholds above. However, it's not really possible to segment any
# scene with 16 thresholds before color thresholds start to overlap heavily.

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)
sensor.set_auto_gain(False)  # must be turned off for color tracking
sensor.set_auto_whitebal(False)  # must be turned off for color tracking
clock = time.clock()

# Only blobs that with more pixels than "pixel_threshold" and more area than "area_threshold" are
# returned by "find_blobs" below. Change "pixels_threshold" and "area_threshold" if you change the
# camera resolution. Don't set "merge=True" becuase that will merge blobs which we don't want here.

img = sensor.snapshot()

merg = framebuf.FrameBuffer(
    bytearray(10 * 100 * 2), img.width(), img.height(), framebuf.MONO_HLSB
)
nomerg = framebuf.FrameBuffer(
    bytearray(10 * 100 * 2), img.width(), img.height(), framebuf.MONO_HLSB
)

while True:
    merg.fill((0, 0, 0))
    nomerg.fill((0, 0, 0))
    notmatches = []
    clock.tick()
    img = sensor.snapshot()
    tru = img.find_blobs(
        thresholds, pixels_threshold=200, area_threshold=200, merge=True
    )
    fals = img.find_blobs(
        thresholds, pixels_threshold=200, area_threshold=200, merge=False
    )
    for blob in tru:
        # These values depend on the blob not being circular - otherwise they will be shaky.
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255, 0, 0))
            img.draw_line(blob.major_axis_line(), color=(0, 255, 0))
            img.draw_line(blob.minor_axis_line(), color=(0, 0, 255))
        # These values are stable all the time.
        img.draw_rectangle(blob.rect())
        img.draw_cross(blob.cx(), blob.cy())
        # Note - the blob rotation is unique to 0-180 only.
        img.draw_keypoints(
            [(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20
        )
        merg.fill_rect(blob.cx(), blob.cy(), blob.w(), blob.h(), (1, 1, 1))
    for blob in fals:
        # These values depend on the blob not being circular - otherwise they will be shaky.
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255, 255, 0))
            img.draw_line(blob.major_axis_line(), color=(0, 255, 255))
            img.draw_line(blob.minor_axis_line(), color=(255, 0, 255))
        # These values are stable all the time.
        img.draw_rectangle(blob.rect())
        img.draw_cross(blob.cx(), blob.cy())
        # Note - the blob rotation is unique to 0-180 only.
        img.draw_keypoints(
            [(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20
        )
        nomerg.fill_rect(blob.cx(), blob.cy(), blob.w(), blob.h(), (1, 1, 1))

    goalblocks = merg.find_blobs(
        thresholds, pixels_thresholds=200, area_threshold=200, merge=False
    )
    print(clock.fps())
