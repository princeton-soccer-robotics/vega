from pyb import UART
from math import atan2, floor, sqrt, cos, sin
import sensor

def HEIGHT_DISTANCE(num):
    return num
    #placeholder height-to-distance conversion curve

def mean(*data):
    return sum(data) / len(data)

def findGoalie(img, thr, inv, xstr, ystr, athr, pixthr):
    #blit = framebuf.FrameBuffer(img, img.width(), img.height(), framebuf.MONO_HLSB)
    comb = img.find_blobs(thresholds = thr, invert = inv, roi = (0,0,320, 240), x_stride = xstr, y_stride = ystr, area_threshold = athr, pixels_threshold = pixthr, merge=True)
    nocomb = img.find_blobs(thresholds = thr, invert = inv, roi = (0,0,320, 240), x_stride = xstr, y_stride = ystr, area_threshold = athr, pixels_threshold = pixthr, merge=False)
    #blit.fill(0)
    result = img.find_blobs(thresholds = [0,0,0,0,0,0], invert = inv, roi = (0,0,320, 240), x_stride = xstr, y_stride = ystr, area_threshold = athr, pixels_threshold = pixthr, merge=False)

def estimateGoalie(blob0, blob1):
    pts = [blob0.vP0, blob0.vP1, blob0.vP2, blob0.vP3, blob1.vP0, blob1.vP1, blob1.vP2, blob1.vP3]
    pts.sort(key=deg)
    return(pts[4].deg - pts[3].deg)

class VPt:
    # constructs with either x and y or with a list of those
    def __init__(self, xin=None, yin=None, lst=None, ptin=None):
        if (xin != None):
            self.x = xin
            self.y = yin
        if lst:
            self.x = lst[0]
            self.y = lst[1]
        if ptin:
            self.x = ptin.x
            self.y = ptin.y
        self.deg = atan2(self.y, self.x)
        self.mag = sqrt(self.y * self.y + self.x * self.x)

    @classmethod
    def from_list(cls, lstin):
        return cls(lst=lstin)

    # returns pythagorean distance from a point (default center) to target point
    def distance_to(self, target=None):
        if not target:
            target = VPt(160, 120)
        return sqrt(
            pow(self.x - target.x, 2) +
            pow(self.y - target.y, 2)
        )
    def out(self):
        return(self.x, self.y)


class BlobCorners:
    # consists of 4 VPts, being the corners
    def __init__(self, p0=None, p1=None, p2=None, p3=None, plist=None):
        if p0:
            self.vP0 = p0
            self.vP1 = p1
            self.vP2 = p2
            self.vP3 = p3
        elif plist:
            self.vP0 = plist[0]
            self.vP1 = plist[1]
            self.vP2 = plist[2]
            self.vP3 = plist[3]
        else:
            print("bad BlobCorners import")


    def out(self):
        return(self.vP0.out(), self.vP1.out(), self.vP2.out(), self.vP3.out())

    def anglerange(self):
        post = sort_corners(self)
        return (min(post.vP0.deg, post.vP1.deg), max(post.vP0.deg, post.vP1.deg))

    def isBetween(self, angle):
        return self.anglerange[0] < angle < self.anglerange[1]


    @classmethod
    def from_listpts(cls, pt0, pt1, pt2, pt3):
        return cls(p0=VPt.from_list(pt0), p1=VPt.from_list(pt1),
            p2=VPt.from_list(pt2), p3=VPt.from_list(pt3))

    @classmethod
    def from_oblob(cls, blobin):
        blcs = blobin.min_corners()
        return cls(p0=VPt.from_list(blcs[0]), p1=VPt.from_list(blcs[1]),
            p2=VPt.from_list(blcs[2]), p3=VPt.from_list(blcs[3]))

    @classmethod
    def from_list(cls, llist):
        return cls(plist=llist)

    @classmethod
    def from_VPts(cls, pt0, pt1, pt2, pt3):
        return cls(p0=pt0, p1=pt1, p2=pt2, p3=pt3)

    @classmethod
    def from_xywh(cls, lblob):
        xh = BlobXYWH.import_blob(lblob)
        cs = xh.to_corners()
        return cls(p0 = cs[0], p1 = cs[1], p2 = cs[2], p3 = cs[3])

    def to_list(self, curve=False):
        if curve:
            return ((round(self.vP0.x), round(self.vP0.y)), (round(self.vP1.x), round(self.vP1.y)), (round(self.vP2.x), round(self.vP2.y)), (round(self.vP3.x), round(self.vP3.y)))
        else:
            return ((self.vP0.x, self.vP0.y), (self.vP1.x, self.vP1.y), (self.vP2.x, self.vP2.y), (self.vP3.x, self.vP3.y))

    def draw_corners(self, imag, farb=(0, 0, 0), gro=1, thicc=1, full=True):
        imag.draw_edges(self.to_list(curve=True),
        color=farb,
        size=gro,
        thickness=thicc,
        fill=full)

    # sorts a cornerblob's points, and returns a list of the sorted points in standard list format
    def sort_corners(self):
        post = sorted((self.vP0, self.vP1, self.vP2,
                       self.vP3), key=VPt.distance_to)
        self.vP0 = post[0]
        self.vP1 = post[1]
        self.vP2 = post[2]
        self.vP3 = post[3]
        return post

    def center(self):
        return (
            mean(
                self.vP0.x,
                self.vP1.x,
                self.vP2.x,
                self.vP3.x
            ), mean(
                self.vP0.y,
                self.vP1.y,
                self.vP2.y,
                self.vP3.y
            )
        )

    # returns the height of the rectangle (distance between top two and bottom two points)
    def height(self):
            self.sort_corners()
            return abs(mean(VPt.distance_to(self.vP0), VPt.distance_to(self.vP1)) -
                            mean(VPt.distance_to(self.vP2) - VPt.distance_to(self.vP3)))
    def size(self):
            return(mean(VPt.distance_to(self.vP0, target=self.vP1),
                            VPt.distance_to(self.vP2, target=self.vP3)* self.height()))
    def width(self):
        self.sort_corners()
        return mean(mean(VPt.distance_to(self.vP0), VPt.distance_to(self.vP1)),
                   mean(VPt.distance_to(self.vP2), VPt.distance_to(self.vP3)))

    def inDist(self):
        self.sort_corners()
        return mean(self.vP0.mag, self.vP1.mag)

    # checks whether each point of two rectangles is similar
    def is_same(self, other, ad=5):
        other.sort_corners()
        self.sort_corners()
        return all((self.vP0.distance_to(other.vp0) < ad,
                    self.vP1.distance_to(other.vp1) < ad,
                    self.vP2.distance_to(other.vp2) < ad,
                    self.vP3.distance_to(other.vp3) < ad))

    def landr(self):
        srtd = self.sort_corners()
        oPt0 = VPt.from_list(0, 0)
        # WHERE IS THIS FUNCTION
        oPt0.setMD(srtd[0].deg(), srtd[0].mag() + 0.5 * self.height())
        oPt1 = VPt.from_list(0, 0)
        oPt1.setMD(srtd[1].deg(), srtd[1].mag() + 0.5 * self.height())
        return (oPt0, oPt1)

    def perimeter(self):
        srtd = self.sort_corners()
        return(
            srtd[0].distance_to(target=srtd[1]) +
            srtd[1].distance_to(target=srtd[2]) +
            srtd[2].distance_to(target=srtd[3]) +
            srtd[3].distance_to(target=srtd[0]))



class BlobXYWH:
    def __init__(self, x=None, y=None, w=None, h=None, r=None, listin=None):
        if not x == None:
            self.x = x
            self.y = y
            self.w = w
            self.h = h
            self.rotation = r
        elif listin and not listin[0] == None:
            self.x = listin[0]
            self.y = listin[1]
            self.w = listin[2]
            self.h = listin[3]
            self.rotation = listin[4]
        else:
            print("bad blob xywh input")


    def out(self):
        return(self.x, self.y, self.w, self.h, self.rotation)

    @classmethod
    def from_list(cls, listin):
        return cls(listin=listin)

    @classmethod
    def import_blob(cls, lob):
        try:
            return cls(x=lob.x, y=lob.y, w=lob.w, h=lob.h, r=lob.rotation)
        except Exception as err:
            print("Exception: problems importing xywh")
            print(err)

    def is_same(self, other, ad=5):
        return all((abs(self.x - other.x) < ad,
                    abs(self.y - other.y) < ad,
                    abs(self.w - other.w) < ad,
                    abs(self.h - other.h) < ad,
                    abs(self.rotation - other.rotation) < ad))

    def center(self):
        return VPt.from_list((self.x + 0.5 * self.w, self.y + 0.5 * self.h))

    def to_corners(self):
        mid = self.center()
        return (
            VPt.from_list((mid.x + 0.5 * self.w * cos(self.rotation), mid.y + 0.5 * self.h * sin(self.rotation))),
            VPt.from_list((mid.x - 0.5 * self.w * cos(self.rotation), mid.y + 0.5 * self.h * sin(self.rotation))),
            VPt.from_list((mid.x + 0.5 * self.w * cos(self.rotation), mid.y - 0.5 * self.h * sin(self.rotation))),
            VPt.from_list((mid.x - 0.5 * self.w * cos(self.rotation), mid.y - 0.5 * self.h * sin(self.rotation))))



    def draw_xywh(img, self, farb=(0, 0, 0), thicc=1, full=True):
        BlobCorners.draw_corners(BlobCorners.from_xywh(self), farb=farb, thicc=thicc, full=full)


class Comms:
    def __init__(self, uart=UART(3, 19200)):
        self.uart = uart

    def write(self, *data):
        self.uart.write("|".join(data) + "\n")


class utils:
    def __init__(
        self,
        gam_cor=0.8,
        contr_cor=0.9,
        bright_cor=0.1,
        lens_str=8,
        lens_zoom=0.55,
        lens_xcorr=30,
        lens_ycorr=40,
    ):
        self.GAM_COR = gam_cor  # GAMMA CORRECTION, INCREASES DARK/LIGHT DIFFERENTIATION
        self.CONTR_COR = (
            contr_cor  # CONTRAST CORRECTION, INCREASES COLOR DIFFERENTIATION
        )
        self.BRIGHT_COR = bright_cor  # BRIGHTNESS CORRECTION, CAN IMPROVE BAD LIGHTING
        self.LENS_STR = lens_str
        self.LENS_ZOOM = lens_zoom
        self.LENS_XCORR = lens_xcorr
        self.LENS_YCORR = lens_ycorr
        self.allow_diff = [10] * 4
        self.uart = UART(3, 19200)

    def set_camera_adjust(self, value):
        # SENSOR IS MISSING
        sensor.set_auto_exposure(value)
        sensor.set_auto_whitebal(value)
        sensor.set_auto_gain(value)

    def contains(self, ob, obs):
        if ob and obs:
            return any(o.is_same(obs) for o in obs)

    # verify that list obs contains an object like ob
    # in this case, check that an image contains a blob

    def matches(self, ob, obss):
        if ob and obss:
            return all(ob.contains(obs) for obs in obss)

    # verify that each frame in obss contains an object like ob

    """
        PLACEHOLDER FUNCTION (CURRENTLY UNUSED), APPLIES PREPROCESSING IMAGE FILTERS
        """

    def correct(self, img):
        img.crop(roi=[22, 0, 276, 240])
        img.lens_corr(
            strength=self.LENS_STR,
            zoom=self.LENS_ZOOM,
            xcorr=self.LENS_XCORR,
            ycorr=self.LENS_YCORR,
        ).gamma_corr(
            gamma=self.GAM_COR, contrast=self.CONTR_COR, brightness=self.BRIGHT_COR
        )  # .cartoon(seed_threshold=0.6, floating_thresholds=0.6)
        return img

    def avg(self, listin):
        return sum(listin) / len(listin)

    def blobdown(self, blist):
        if blist:
            bblob = [0, 0, 0, 0, 0]
            for i in blist:
                bblob[0] += i.x()
                bblob[1] += i.y()
                bblob[2] += i.w()
                bblob[3] += i.h()
                bblob[4] += i.rotation()
            bblob[0] = floor(bblob[0] / len(blist))
            bblob[1] = floor(bblob[1] / len(blist))
            bblob[2] = floor(bblob[2] / len(blist))
            bblob[3] = floor(bblob[3] / len(blist))
            bblob[4] = floor(bblob[4] / len(blist))
            return bblob

    """
        DEPRECATED
        """


    def draw_shit(self, r, img):
        img.draw_line(r.major_axis_line(), color=(0, 255, 0))
        img.draw_line(r.minor_axis_line(), color=(0, 0, 255))
        img.draw_rectangle(r.rect())
        img.draw_cross(r.cx(), r.cy())

    def send_to_teensy(self, *data):
        self.uart.write("|".join(str(x) for x in data) + "\n")
