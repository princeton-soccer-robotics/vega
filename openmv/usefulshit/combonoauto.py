import sensor, image, time, sys, math
from pyb import UART


sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time = 2000)
clock = time.clock()

'''
PREVENTS CAMERA FROM AUTOMATICALLY CORRECTING IMAGE QUALITIES, PRESERVING LIGHTING
'''
def setCameraAdjust(value):
    sensor.set_auto_exposure(value)
    sensor.set_auto_whitebal(value)
    sensor.set_auto_gain(value)

sensor.setCameraAdjust(False)

sensor.set_auto_gain(False) # must be turned off for color tracking
sensor.set_auto_whitebal(False) # must be turned off for color tracking

def sendToTeensy(uart, *data):
    uart.write("|".join(data) + "\n")
    print("your mom is gay")


'''
VARIOUS CONSTANTS
'''
GAM_COR = 0.8 # GAMMA CORRECTION, INCREASES DARK/LIGHT DIFFERENTIATION
CONTR_COR = 0.9 # CONTRAST CORRECTION, INCREASES COLOR DIFFERENTIATION
BRIGHT_COR = 0.1 # BRIGHTNESS CORRECTION, CAN IMPROVE BAD LIGHTING
A_ONE = (30, 0, 260, 80) # UNUSED & DEPRECATED
allowdiff = [10] * 4


'''
THRESHOLDS FOR BLOB FINDER TO USE
'''
thresholds = [(32, 60, -21, 18, 32, 84)]
            (0, 100, -128, 71, -128, -28)] # generic_blue_thresholds


'''
CONVERTS RECTANGLE TO LIST FOR ITERABILITY
'''
def tolist(rectin):
    return(rectin.x(), rectin.y(), rectin.w(), rectin.h())

def isSame(blob1, blob2):
    if blob1 and blob2:
        return all(abs(b1 - b2) < ad for b1, b2, ad in zip(tolist(blob1), tolist(blob2), allowdiff))
    # verify that one blob is similar to the other

def contains(ob, obs):
    if ob and obs:
        return any(isSame(o, ob) for o in obs)
    # verify that list obs contains an object like ob
    # in this case, check that an image contains a blob

def matches(ob, obss):
    if ob and obss:
        return all(contains(ob, obs) for obs in obss)
    # verify that each frame in obss contains an object like ob

'''
DECLARES IMG TYPE FOR USE IN FUNCTIONS
'''

img = sensor.snapshot()


'''
PLACEHOLDER FUNCTION (CURRENTLY UNUSED), APPLIES PREPROCESSING IMAGE FILTERS
'''
def correct(img):
    img.crop(roi=[22, 0, 276, 240])
    img.lens_corr(strength = 8, zoom = 0.55, xcorr=30, ycorr=40).gamma_corr(gamma = GAM_COR, contrast = CONTR_COR, brightness = BRIGHT_COR)# .cartoon(seed_threshold=0.6, floating_thresholds=0.6)
    return img


'''
DEPRECATED PLACEHOLDER FUNCTION, POST-PROCESSES RECOGNIZED BLOBS
'''
def evalrect(rectin, color = "either"):
    imgur = img.get_statistics(roi = tolist(rectin))
    if imgur.l_mean() < -80 and imgur.b_mean() < 64 and (color == 'blue' or color == 'either'):
        return True
    elif imgur.l_mean() > 64 and -20 > imgur.a_mean() > 32 and imgur.b_mean() > 64 and (color == 'yellow' or color == 'either'):
        return True
    else:
        return False

def biggest(listin, spot):
    bigger = 0
    for i in listin:
        if i[spot] > bigger:
            bigger = i[spot]

def smaller(listin, spot):
    smaller = 128
    for i in listin:
        if i[spot] < bigger:
            bigger = i[spot]

def blobdown(blist):
    bblob = [0,0,0,0]
    for i in blist:
        bblob[0] += i.x()
        bblob[1] += i.y()
        bblob[2] += i.width()
        bblob[3] += i.height()
    return bblob

'''
DRAWS LABELS FOR BLOBS
'''
def drawshit(r):
    global img
    img.draw_line(r.major_axis_line(), color=(0,255,0))
    img.draw_line(r.minor_axis_line(), color=(0,0,255))
    img.draw_rectangle(r.rect())
    img.draw_cross(r.cx(), r.cy())

'''
FINDS THE ENEMY BY CHECKING WHICH COLOR IS HIGHER IN THE FOV, PLEASE ORIENT ROBOT CORRECTLY ON STARTUP
'''
img = correct(sensor.snapshot(()))
b1s = img.find_blobs(thresholds[0], pxels_threshold=200, area_threshold=200, merge=True)
b2s = img.find_blobs(thresholds[1], pxels_threshold=200, area_threshold=200, merge=True)
for blob in b1s:
    b1y += blob.cy()
for blob in b2s:
    b2y += blob.cy()

if b1y/len(b1s) > b2y/len(b2s):
    enemy = 0
else:
    enemy = 1


'''
MAIN LOOP
'''

print("*bops* color 1 is done!")
while(True):
    img = correct(sensor.snapshot(()))
    clock.tick()

    '''
    COLOR ONE
    '''
    b1s = img.find_blobs(thresholds[0], pixels_threshold=200, area_threshold=200, merge=True)
    for blob in b1s:
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255,0,0))
            img.draw_line(blob.major_axis_line(), color=(0,255,0))
            img.draw_line(blob.minor_axis_line(), color=(0,0,255))
        img.drawshit(blob)


    '''
    COLOR TWO
    '''

    b2s = img.find_blobs(thresholds[1], pixels_threshold=200, area_threshold=200, merge=True)
    for blob in b2s:
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255,0,0))
            img.draw_line(blob.major_axis_line(), color=(0,255,0))
            img.draw_line(blob.minor_axis_line(), color=(0,0,255))
        img.drawshit(blob)
    if enemy == 0:
        bes = b1s
    else:
        bes = b2s

    potgoalies = img.find_blobs(thresholds[0], roi=(blobdown(bes)) pixels_threshold=200, area_threshold=200, invert=True, merge=True)
    img.draw_rectangle(blobdown(potgoalies), color = (0,0,0), fill=True)
    eqs = bs[0].cx()- wide/2
    ygr = bs[0].cy()-high/2
    angel = fmod(450+180/pi*(atan2(ygr, eqs)), 360)
    sendToTeensy(uart, str(angel))
    img.draw_string(int(eqs + 3 + (wide/2)),
        int(ygr + 3 + (high/2)),
        str(angel),
        color=(255,0,0))
