import sensor, image, time, sys, math

allowdiff = [5] * 4
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)
clock = time.clock()

threshold_index = 1  # 0 for red, 1 for green, 2 for blue

# Color Tracking Thresholds (L Min, L Max, A Min, A Max, B Min, B Max)
# The below thresholds track in general red/green/blue things. You may wish to tune them...
thresholds = [
    (0, 0, 0, 0, 0, 0),
    (38, 100, -20, 15, 20, 128),
]  # generic_blue_thresholds


def tolist(rectin):
    return (rectin.x(), rectin.y(), rectin.w(), rectin.h())


def isSame(blob1, blob2):
    if blob1 and blob2:
        return all(
            abs(b1 - b2) < ad
            for b1, b2, ad in zip(tolist(blob1), tolist(blob2), allowdiff)
        )
    # verify that one blob is similar to the other


def contains(ob, obs):
    if ob and obs:
        return any(isSame(o, ob) for o in obs)
    # verify that list obs contains an object like ob
    # in this case, check that an image contains a blob


def matches(ob, obss):
    if ob and obss:
        return all(contains(ob, obs) for obs in obss)
    # verify that each frame in obss contains an object like ob


img = sensor.snapshot()


def evalrect(rectin, color="either"):
    return True
    imgur = img.get_statistics(roi=tolist(rectin))
    if (
        imgur.l_mean() < -80
        and imgur.b_mean() < 64
        and (color == "blue" or color == "either")
    ):
        return True
    elif (
        imgur.l_mean() > 64
        and -20 > imgur.a_mean() > 32
        and imgur.b_mean() > 64
        and (color == "yellow" or color == "either")
    ):
        return True
    else:
        return False


done = None  # no rect found yet
pastrects = []
goodrects = pastrects

while not done:
    clock.tick()
    img = sensor.snapshot().lens_corr(strength=5, zoom=0.7)
    # pastrects is a list of frames, each of which contains a list of rectangles, each of which has xywh
    # `threshold` below should be set to a high enough value to filter out noise
    # rectangles detected in the image which have low edge magnitudes. Rectangles
    # have larger edge magnitudes the larger and more contrasty they are...
    if len(pastrects) > 10:  # trim off the oldest image, keeping the list at len 10
        pastrects = pastrects[1:]
    rs = img.find_blobs(
        thresholds, pixels_threshold=200, area_threshold=200, merge=True
    )
    img.draw_rectangle(0, 0, 320, 120, color=(255, 0, 0))
    if rs:
        # img.draw_rectangle(imgur.x(), imgur.y(), imgur.w(), imgur.h(), color = image.lab_to_rgb((imgur.l_mean(), imgur.a_mean(), imgur.b_mean())), thickness = 3)
        for r in rs:  # for each rectangle
            # These values depend on the blob not being circular - otherwise they will be shaky.
            stats = img.get_statistics(roi=r.rect())
            img.draw_rectangle(
                r.rect(),
                color=(
                    (image.lab_to_rgb(stats.l_mean(), stats.a_mean(), stats.b_mean()))
                ),
                fill=True,
            )
            print(stats.l_mean(), stats.a_mean(), stats.b_mean())
            img.draw_line(r.major_axis_line(), color=(0, 255, 0))
            img.draw_line(r.minor_axis_line(), color=(0, 0, 255))
            # These values are stable all the time.
            img.draw_rectangle(r.rect())
            img.draw_cross(r.cx(), r.cy())
            # Note - the blob rotation is unique to 0-180 only.
            img.draw_keypoints(
                [(r.cx(), r.cy(), int(math.degrees(r.rotation())))], size=20
            )
            if matches(r, pastrects):  # and evalrect(r):
                print(r)
                img.draw_rectangle(r.rect(), color=(255, 0, 0), thickness=5)
                goodrects.append(
                    r
                )  # breaks out of the loop, declares r as a "good" goal
                done = 1

    pastrects.append(rs)  # add latest image to list
print("*bops* color 1 is done!")
