import sensor, image, time, sys

allowdiff = [5] * 4
sensor.reset()
sensor.set_pixformat(sensor.RGB565)  # grayscale is faster (160x120 max on OpenMV-M7)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)
clock = time.clock()


def tolist(rectin):
    return (rectin.x(), rectin.y(), rectin.w(), rectin.h())


def isSame(blob1, blob2):
    if blob1 and blob2:
        return all(
            abs(b1 - b2) < ad
            for b1, b2, ad in zip(tolist(blob1), tolist(blob2), allowdiff)
        )
    # verify that one blob is similar to the other


def contains(ob, obs):
    if ob and obs:
        return any(isSame(o, ob) for o in obs)
    # verify that list obs contains an object like ob
    # in this case, check that an image contains a blob


def matches(ob, obss):
    if ob and obss:
        return all(contains(ob, obs) for obs in obss)
    # verify that each frame in obss contains an object like ob


img = sensor.snapshot()


def evalrect(rectin, color):
    imgur = img.get_statistics()
    if (
        imgur.l_mean(roi=rectin) < -80
        and imgur.b_mean(roi=rectin) < 64
        and color == "blue"
    ):
        return True
    if (
        imgur.l_mean(roi=rectin) > 64
        and -20 > imgur.a_mean(roi=rectin) > 32
        and imgur.b_mean(roi=rectin) > 64
        and color == "yellow"
    ):
        return True


done = None  # no rect found yet
pastrects = []
goodrects = pastrects

while not done:
    clock.tick()
    img = sensor.snapshot()
    # pastrects is a list of frames, each of which contains a list of rectangles, each of which has xywh
    # `threshold` below should be set to a high enough value to filter out noise
    # rectangles detected in the image which have low edge magnitudes. Rectangles
    # have larger edge magnitudes the larger and more contrasty they are...
    if len(pastrects) > 10:  # trim off the oldest image, keeping the list at len 10
        pastrects = pastrects[1:]
    rs = img.find_rects(roi=(0, 0, 160, 120), threshold=10000)
    img.draw_rectangle(0, 0, 160, 120, color=(255, 0, 0))
    if rs:
        for r in rs:  # for each rectangle
            if evalrect(r, "blue"):
                img.draw_rectangle(r.rect(), color=(255, 0, 0))  # show it in fbuf
                print(r)

                if matches(r, pastrects):
                    goodrects.append(
                        r
                    )  # breaks out of the loop, declares r as a "good" goal
                    done = 1
                    break

    pastrects.append(rs)  # add latest image to list
print("*bops* color 1 is done!")

while not done:
    clock.tick()
    img = sensor.snapshot()
    # pastrects is a list of frames, each of which contains a list of rectangles, each of which has xywh
    # `threshold` below should be set to a high enough value to filter out noise
    # rectangles detected in the image which have low edge magnitudes. Rectangles
    # have larger edge magnitudes the larger and more contrasty they are...
    if len(pastrects) > 10:  # trim off the oldest image, keeping the list at len 10
        pastrects = pastrects[1:]
    rs = img.find_rects(roi=(0, 120, 160, 120), threshold=10000)
    img.draw_rectangle(0, 120, 160, 120, color=(255, 0, 0))
    if rs:
        for r in rs:  # for each rectangle
            if evalrect(r, "yellow"):
                img.draw_rectangle(r.rect(), color=(255, 0, 0))  # show it in fbuf
                print(r)

                if matches(r, pastrects):
                    goodrects.append(
                        r
                    )  # breaks out of the loop, declares r as a "good" goal
                    done = 1
                    break

    pastrects.append(rs)  # add latest image to list
print("*bops* color 2 is done!")
