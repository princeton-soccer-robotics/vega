# Untitled - By: Auste - Wed Feb 26 2020

import sensor, image, time, pyb

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)

clock = time.clock()


def callback(line):
    print("called", line)


extint = pyb.ExtInt("P7", pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback)

while True:
    clock.tick()
    img = sensor.snapshot()
    print(clock.fps())
    time.sleep(10000)
