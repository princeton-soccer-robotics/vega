import sensor, image, time
import random
from pyb import I2C

i2c = I2C(2)
i2c.init(I2C.MASTER)
i2c.scan()

teensy = 8
code = "kancho"

sensor.reset()  # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565)  # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.QVGA)  # Set frame size to QVGA (320x240)
sensor.skip_frames(time=2000)  # Wait for settings take effect.
clock = time.clock()  # Create a clock object to track the FPS.

while True:
    clock.tick()  # Update the FPS clock.
    if i2c.is_ready(teensy):
        amount = random.randrange(2, 5)
        print("sending: " + code + str(amount))
        i2c.send(code + amount, addr=teensy, timeout=500)
    else:
        print("uwu")
    time.sleep(1000)
