# Untitled - By: Auste - Sat Feb 8 2020

import sensor, image, time

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QQVGA)
sensor.skip_frames(time = 2000)

clock = time.clock()

while(True):
    clock.tick()
    img = sensor.snapshot()
    img.find_rects()
    for rect in img.find_rects(threshold=10000
        img.draw_rectangle(rect.rect())
    print(clock.fps())
