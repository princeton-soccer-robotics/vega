# Untitled - By: Auste - Sat Apr 11 2020

import sensor, image, time, random
from pyb import Pin

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)

clock = time.clock()
picnumber = 0
pin0 = Pin("P0", Pin.IN, Pin.PULL_UP)
pin1 = Pin("P1", Pin.IN, Pin.PULL_UP)


while True:
    clock.tick()
    if pin0.value() == 0:
        sensor.snapshot().save("img%d.bmp" % picnumber)
        print("gotcha", picnumber)
        picnumber += 1
    if pin1.value() == 0 and picnumber != 0:
        pull = random.randint(0, picnumber - 1)
        print("drawn! ", pull)
        myImage = image.Image("img%d.bmp" % pull, copy_to_fb=True)
