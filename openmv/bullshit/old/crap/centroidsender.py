# Untitled - By: Auste - Thu Aug 29 2019

import sensor, image, time, pyb, math
from pyb import UART
import json

threshold_index = 2  # 0 for red, 1 for green, 2 for blue
# Color Tracking Thresholds (L Min, L Max, A Min, A Max, B Min, B Max)
# The below thresholds track in general red/green/blue things. You may wish to tune them...
thresholds = [
    (30, 100, 15, 127, 15, 127),  # generic_red_thresholds
    (70, 110, -20, 20, -20, 20),  # mark yellow
    (33, 83, 125, 175, -45, 5),  # blue down
    (0, 30, 0, 64, -128, 0),
]  # generic_blue_thresholds
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)
sensor.set_auto_gain(False)  # must be turned off for color tracking
sensor.set_auto_whitebal(False)  # must be turned off for color tracking
clock = time.clock()

uart = UART(3, 9600, timeout_char=1000)  # init with given baudrate
uart.init(
    9600, bits=8, parity=None, stop=1, timeout_char=1000
)  # init with given parameters


def thresholds(r, g, b, rt, gt, bt, pm=20, pmt=20):
    l, a, b = image.rgb_to_lab((r, g, b))
    lt, at, bt = image.rgb_to_lab((rt, gt, bt))
    return (
        l - pm,
        l + pm,
        a - pm,
        a + pm,
        b - pm,
        b + pm,
        lt - pmt,
        lt + pmt,
        at - pmt,
        at + pmt,
        bt - pmt,
        bt + pmt,
    )


th = thresholds(90, 140, 165, 156, 113, 66, 15, 15)  # blue+yellow


while True:
    clock.tick()
    img = sensor.snapshot()
    img.gaussian(1)
    for blob in img.find_blobs(
        [th], pixels_threshold=200, area_threshold=200, merge=True
    ):
        # These values depend on the blob not being circular - otherwise they will be shaky.
        img.draw_edges(blob.min_corners(), color=(255, 0, 0))
        img.draw_line(blob.major_axis_line(), color=(0, 255, 0))
        img.draw_line(blob.minor_axis_line(), color=(0, 0, 255))
        # These values are stable all the time.
        img.draw_rectangle(blob.rect())
        img.draw_cross(blob.cx(), blob.cy())
    print(clock.fps())
    # Note - the blob rotation is unique to 0-180 only.
#         text = json.dumps(str(blob.cy()) + " x, " + str(blob.cy()) + " y")
#          print(text)
#           for c in text + '\n':
#               uart.write(c)
#               time.sleep(15)

# img.draw_keypoints([(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20)
# print(clock.fps())


156, 113, 66
