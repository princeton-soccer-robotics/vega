# Untitled - By: Auste - Thu Aug 29 2019

import sensor, image, time, pyb
from pyb import UART
import json

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)

clock = time.clock()

uart = UART(3, 112500, timeout_char=1000)  # init with given baudrate
uart.init(
    112500, bits=8, parity=None, stop=1, timeout_char=1000
)  # init with given parameters

while True:
    clock.tick()
    img = sensor.snapshot()
    # print(clock.fps())
    text = json.dumps(clock.fps())
    for c in text + "\n":
        uart.write(c)
        print(c)
        time.sleep(15)
