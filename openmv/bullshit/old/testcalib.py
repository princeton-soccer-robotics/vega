# Hello World Example
#
# Welcome to the OpenMV IDE! Click on the green run arrow button below to run the script!

import sensor, image, time, math

sensor.reset()  # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565)  # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.QVGA)  # Set frame size to QVGA (320x240)
sensor.skip_frames(time=2000)  # Wait for settings take effect.
clock = time.clock()  # Create a clock object to track the FPS.
sensor.set_auto_exposure(True)
sensor.set_auto_whitebal(True)
sensor.set_auto_gain(True)
pm = 15

time.sleep(2000)

img = sensor.snapshot()
bluecall = img.get_statistics(roi=(155, 115, 10, 10))

blue = [bluecall.l_mode(), bluecall.a_mode(), bluecall.b_mode()]

time.sleep(2000)

yellowcall = img.get_statistics(roi=(155, 115, 10, 10))

yellow = [
    (
        (yellowcall.l_mode() + 20),
        (yellowcall.l_mode() - 20),
        (yellowcall.a_mode() + 20),
        (yellowcall.a_mode() - 20),
        (yellowcall.b_mode() + 20),
        (yellowcall.b_mode() - 20),
    ),
    (
        (yellowcall.l_mode() + 20),
        (yellowcall.l_mode() - 20),
        (yellowcall.a_mode() + 20),
        (yellowcall.a_mode() - 20),
        (yellowcall.b_mode() + 20),
        (yellowcall.b_mode() - 20),
    ),
]

sensor.set_auto_exposure(False)
sensor.set_auto_whitebal(False)
sensor.set_auto_gain(False)

# Single Color RGB565 Blob Tracking Example
#
# This example shows off single color RGB565 tracking using the OpenMV Cam.


threshold_index = 0  # 0 for red, 1 for green, 2 for blue

# Color Tracking Thresholds (L Min, L Max, A Min, A Max, B Min, B Max)
# The below thresholds track in general red/green/blue things. You may wish to tune them...
thresholds = [
    (
        (yellowcall.l_mode() + 20),
        (yellowcall.l_mode() - 20),
        (yellowcall.a_mode() + 20),
        (yellowcall.a_mode() - 20),
        (yellowcall.b_mode() + 20),
        (yellowcall.b_mode() - 20),
    ),  # generic_red_thresholds
    (
        (bluecall.l_mode() + 20),
        (bluecall.l_mode() - 20),
        (bluecall.a_mode() + 20),
        (bluecall.a_mode() - 20),
        (bluecall.b_mode() + 20),
        (bluecall.b_mode() - 20),
    ),  # generic_green_thresholds
    (0, 30, 0, 64, -128, 0),
]  # generic_blue_thresholds


# Only blobs that with more pixels than "pixel_threshold" and more area than "area_threshold" are
# returned by "find_blobs" below. Change "pixels_threshold" and "area_threshold" if you change the
# camera resolution. "merge=True" merges all overlapping blobs in the image.

while True:
    clock.tick()
    img = sensor.snapshot()
    for blob in img.find_blobs(
        [thresholds[0]], pixels_threshold=200, area_threshold=200, merge=True
    ):
        # These values depend on the blob not being circular - otherwise they will be shaky.
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255, 0, 0))
            img.draw_line(blob.major_axis_line(), color=(0, 255, 0))
            img.draw_line(blob.minor_axis_line(), color=(0, 0, 255))
        # These values are stable all the time.
        img.draw_rectangle(blob.rect())
        img.draw_cross(blob.cx(), blob.cy())
        # Note - the blob rotation is unique to 0-180 only.
        img.draw_keypoints(
            [(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20
        )
    print(clock.fps())
