# Basic UART communications between OpenMV and Arduino Uno.

# 1) Wire up your OpenMV Cam to your Arduino Uno like this:
#
# OpenMV Cam Ground Pin   ----> Arduino Ground
# OpenMV Cam UART3_TX(P4) ----> Arduino Uno UART_RX(0)
# OpenMV Cam UART3_RX(P5) ----> Arduino Uno UART_TX(1)

# 2) Uncomment and upload the following sketch to Arduino:
#
# void setup() {
#   // put your setup code here, to run once:
#   Serial.begin(19200);
# }
#
# void loop() {
#   // put your main code here, to run repeatedly:
#   if (Serial.available()) {
#     // Read the most recent byte
#     byte byteRead = Serial.read();
#     // ECHO the value that was read
#     Serial.write(byteRead);
#   }
# }

# 3) Run the following script in OpenMV IDE:

import time, sensor, pyb
from pyb import UART

import sensor, image, time

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time=2000)

clock = time.clock()

uart = UART(3, 19200)


# UART 3, and baudrate.


while True:
    uart.write("420\n")
    clock.tick()
    img = sensor.snapshot()
    print(clock.fps())
