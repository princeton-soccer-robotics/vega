# Hello World Example
#
# Welcome to the OpenMV IDE! Click on the green run arrow button below to run the script!

import sensor, image, time

sensor.reset()  # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565)  # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.QVGA)  # Set frame size to QVGA (320x240)
sensor.skip_frames(time=2000)  # Wait for settings take effect.
clock = time.clock()  # Create a clock object to track the FPS.

time.sleep(2000)

img = sensor.snapshot()
bluecall = img.get_statistics(roi=(155, 115, 10, 10))

blue = [bluecall.l_mode(), bluecall.a_mode(), bluecall.b_mode()]

time.sleep(5000)

yellowcall = img.get_statistics(roi=(155, 115, 10, 10))

yellow = [yellowcall.l_mode(), yellowcall.a_mode(), yellowcall.b_mode()]

while True:
    clock.tick()  # Update the FPS clock.
    img = sensor.snapshot()  # Take a picture and return the img.
    print(
        yellowcall, bluecall
    )  # Note: OpenMV Cam runs about half as fast when connected
    # to the IDE. The FPS should increase once disconnected.
