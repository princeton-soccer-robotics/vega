import sensor
import image
import time
import math
import ustruct
import micropython
import pyb
from pyb import UART


def sendToTeensy(comm, *data):
    comm.write("|".join(data) + "\n")


uart = UART(3, 19200)
clock = time.clock()

angle = 0

while True:
    sendToTeensy(uart, str(angle))

    angle = (angle + 3) % 360
    delay(100)

