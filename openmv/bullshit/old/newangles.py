# Hello World Example
#
# Welcome to the OpenMV IDE! Click on the green run arrow button below to run the script!

import sensor, image, time, math
import pyb
from pyb import UART

if switch on:
    enemycolor = blue
else:
    enemycolor = yellow

def thresholds(r,g,b, pm=20, ):
    l, a, b = image.rgb_to_lab((r, g, b))
    return ((l-pm, l+pm, a-pm, a+pm, b-pm, b+pm))


'ABSOLUTE LOCATION'

def absloc():
    mone = tan(180-goal1())
    mtwo = tan(goal2()-180)

    return fieldheight/(mone+mtwo), ((fieldheight/(mone+mtwo)*mone)+150)

def angletoenemy:
    return atan(blob.cy(), blob.cx())


BLUE_REGION = (135, 220, 30, 10)
YELLOW_REGION = (135, 40, 30, 10)

sensor.reset()                      # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565) # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.QVGA)   # Set frame size to QVGA (320x240)
sensor.skip_frames(time = 2000)     # Wait for settings take effect.
clock = time.clock()                # Create a clock object to track the FPS.
sensor.set_auto_exposure(True)
sensor.set_auto_whitebal(True)
sensor.set_auto_gain(True)
blobdev = 10 # how far blobs are allowed to deviate

time.sleep(2000)

img = sensor.snapshot()
bluecall = img.get_statistics(roi = BLUE_REGION)

blue = [bluecall.l_mode(), bluecall.a_mode(), bluecall.b_mode()]

time.sleep(2000)

yellowcall = img.get_statistics(roi = YELLOW_REGION)
LMode_Y = yellowcall.l_mode()
LMode_B = bluecall.l_mode()
AMode_Y = yellowcall.a_mode()
AMode_B = bluecall.l_mode()
BMode_Y = yellowcall.b_mode()
BMode_B = bluecall.l_mode()

yellow = [
    ((LMode_Y + blobdev), (LMode_Y - blobdev), (AMode_Y + blobdev), (AMode_Y - blobdev), (BMode_Y + blobdev), (BMode_Y - blobdev))
]

blue = [
    ((LMode_B + blobdev), (LMode_B - blobdev), (AMode_B + blobdev), (AMode_B - blobdev), (BMode_B + blobdev), (BMode_B - blobdev))
]

sensor.set_auto_exposure(False)
sensor.set_auto_whitebal(False)
sensor.set_auto_gain(False)

# Color Tracking Thresholds (L Min, L Max, A Min, A Max, B Min, B Max)
# The below thresholds track in general red/green/blue things. You may wish to tune them...
thresholds = [
    yellow[0],              # generic_red_thresholds
    blue[0],                # generic_green_thresholds
    (0, 30, 0, 64, -128, 0) # ???
]


# Only blobs that with more pixels than "pixel_threshold" and more area than "area_threshold" are
# returned by "find_blobs" below. Change "pixels_threshold" and "area_threshold" if you change the
# camera resolution. "merge=True" merges all overlapping blobs in the image.

while(True):
    clock.tick()
    img = sensor.snapshot()
    for blob in img.find_blobs(thresholds, pixels_threshold=200, area_threshold=200, merge=True):
        # These values depend on the blob not being circular - otherwise they will be shaky.
        if blob.elongation() > 0.5:
            img.draw_edges(blob.min_corners(), color=(255,0,0))
            img.draw_line(blob.major_axis_line(), color=(0,255,0))
            img.draw_line(blob.minor_axis_line(), color=(0,0,255))
        # These values are stable all the time.
        img.draw_rectangle(blob.rect())
        img.draw_cross(blob.cx(), blob.cy())
        # Note - the blob rotation is unique to 0-180 only.
        img.draw_keypoints([(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20)
    print(clock.fps())
    img.draw_rectangle(135, 200, 30, 10)
    img.draw_rectangle(135, 40, 30, 10)

    x = blob.cx()
    y = blob.cy()

    #if x>0 and y>0:
    #    print(math.atan(y/x))
    #if x<0 and y>0:
    #    print(math.atan(x/y)+90)
    #if x<0 and y<0:
    #    print(math.atan(y/x)+180)
    #if x>0 and y<0:
    #    print(math.atan(x/y)+270)




#ADJUST FOR BLUE AND YELLOW GOAL1 VS GOAL2
#ADJUST FIELD SIZE IN CM

threshold_index = 2 # 0 for red, 1 for green, 2 for blue
# Color Tracking Thresholds (L Min, L Max, A Min, A Max, B Min, B Max)
# The below thresholds track in general red/green/blue things. You may wish to tune them...
thresholds = [
    (30, 100, 15, 127, 15, 127),# generic_red_thresholds
    (70, 110, -20, 20, -20, 20),#mark yellow
    (33, 83, 125, 175, -45, 5), #blue down
    (0, 30, 0, 64, -128, 0)     # generic_blue_thresholds
]
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time = 2000)
sensor.set_auto_gain(False) # must be turned off for color tracking
sensor.set_auto_whitebal(False) # must be turned off for color tracking
clock = time.clock()

uart = UART(3, 9600, timeout_char=1000)                         # init with given baudrate
uart.init(9600, bits=8, parity=None, stop=1, timeout_char=1000) # init with given parameters

th = thresholds(90, 140, 165, 156) #blue+y
th2 = thresholds(113, 66, 15, 15) #b+yellow


predlist = (0, 0 ,0, 0, 0, 0, 0,)
fieldheight = 300
fieldwidth = 200

while(True):
    clock.tick()
    img = sensor.snapshot()
    img.gaussian(1)
    blobavglist1 = []
    blobavglist2 = []


    'BLUE CALCS'


    for blob in img.find_blobs([th], pixels_threshold=200, area_threshold=200, merge=True):
        # These values depend on the blob not being circular - otherwise they will be AQ    shaky.
        img.draw_edges(blob.min_corners(), color=(255,0,0))
        img.draw_line(blob.major_axis_line(),  color=(0,255,0))
        img.draw_line(blob.minor_axis_line(), color=(0,0,255))
        # These values are stable all the time.
        img.draw_rectangle(blob.rect())
        img.draw_cross(blob.cx(), blob.cy())
        x = blob.cx();
        y = blob.cy();

        if x>0 and y>0:
            blobavglist1.append(math.atan(y/x))
        if x<0 and y>0:
            blobavglist1.append(math.atan(x/y)+90)
        if x<0 and y<0:
            blobavglist1.append(math.atan(y/x)+180)
        if x>0 and y<0:
            blobavglist1.append(math.atan(x/y)+270)

    #predlist1 = math.avg(blobavglist1, predlist1[1], predlist1[2], predlist1[3], predlist1[4], predlist1[5], predlist1[5], predlist1[6])
    #predslope1 = math.sum(predlist1)/7
    #predxint1 = predlist1[0]


    'YELLOW CALCS'

    for blob in img.find_blobs([th2], pixels_threshold=200, area_threshold=200, merge=True):
        # These values depend on the blob not being circular - otherwise they will be shaky.
        img.draw_edges(blob.min_corners(), color=(255,0,0))
        img.draw_line(blob.major_axis_line(), color=(0,255,0))
        img.draw_line(blob.minor_axis_line(), color=(0,0,255))
        # These values are stable all the time.
        img.draw_rectangle(blob.rect())
        img.draw_cross(blob.cx(), blob.cy())

        x = blob.cx()
        y = blob.cy()
        angle = atan(y/x)

        if x>0 and y>0:
            blobavglist2.append(math.atan(y/x))
        if x<0 and y>0:
            blobavglist2.append(math.atan(x/y)+90)
        if x<0 and y<0:
            blobavglist2.append(math.atan(y/x)+180)
        if x>0 and y<0:
            blobavglist2.append(math.atan(x/y)+270)

    #predlist2 = (math.avg(blobavglist2), predlist2[1], predlist2[2], predlist2[3], predlist2[4], predlist2[5], predlist2[5], predlist2[6])
    #predslope2 = math.sum(predlist2)/7
    #predxint2 = predlist1[0]
