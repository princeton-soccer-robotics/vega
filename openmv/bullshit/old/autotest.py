# WARNING: based codebase ahead. Proceed with caution.

import sensor
import image
import time
import math
import ustruct
import micropython
import pyb
from pyb import UART

picnumber = 0
#0 for capture, 1 for testing
mode = 0

# CENTER describes the center of the image (QVGA format)
CENTER = (160, 120)

pin0 = Pin('P0', Pin.IN, Pin.PULL_UP)

# The regions of interest to get colors for (sections of the camera's image to find colors for)
BLUE_REGION = (160, 15, 30, 5)
YELLOW_REGION = (160, 225, 30, 5)

PIX_THR = 100 #def 200
AREA_THR = 100 #def 400

# How far blobs are allowed to deviate from the specified color
BLOBDEV_L = 13
BLOBDEV_A = 5
BLOBDEV_B = 9

GAM_COR = 0.8
CONTR_COR = 0.8
BRIGHT_COR = 0.0

EDGE_COLOR = (255, 0, 0)
MAJOR_AXIS_COLOR = (0, 255, 0)
MINOR_AXIS_COLOR = (0, 0, 255)

def takePic():
    global img
    img = sensor.snapshot().gamma_corr(gamma = GAM_COR, contrast = CONTR_COR, brightness = BRIGHT_COR)
    img.gaussian(2)

def centerX(x):
    return x - CENTER[0]

def centerY(y):
    return y - CENTER[1]

# avg returns the average of a list's values. O(n)!
def avg(list):
    return sum(list)/len(list)

# toLAB converts RGB thresholds to LAB. Necessary for the camera
def toLAB(r, g, b, pm=20, lm=25):
    l, a, b = image.rgb_to_lab((r, g, b))
    return ((l-lm, l+lm, a-pm, a+pm, b-pm, b+pm))


# setCameraAdjust sets the auto exposure, white balance, and gain variables on or off
def setCameraAdjust(value):
    sensor.set_auto_exposure(value)
    sensor.set_auto_whitebal(value)
    sensor.set_auto_gain(value)


def draw_blob(blob, img):
    img.draw_edges(blob.min_corners(), color=EDGE_COLOR)
    img.draw_line(blob.major_axis_line(), color=MAJOR_AXIS_COLOR)
    img.draw_line(blob.minor_axis_line(), color=MINOR_AXIS_COLOR)
    img.draw_rectangle(blob.rect())
    img.draw_cross(blob.cx(), blob.cy())
    img.draw_keypoints(
        [(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20)
    img.draw_rectangle(blob.rect())
    img.draw_cross(blob.cx(), blob.cy())
    img.draw_keypoints(
        [(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20)


# sendToTeensy any amount of data to the teensy
def sendToTeensy(comm, *data):
    comm.write("|".join(data) + "\n")

def trueAngle(angle):
    angleOut = angle - 270
    if angleOut < 0:
        angleOut = angleOut + 360
    return angleOut

def angleBetween(blob1, blob2):
    angleOut = angleTo(blob1) + angleTo(blob2)
    angleOut = angleOut / 2
    if abs(angleTo(blob1) - angleTo(blob2)) > 180:
        angleOut = angleOut + 180
    return angleOut

def angleTo(blob):
    if centerX(blob[0]) == 0:
        return 0
    angleOut = math.degrees(math.atan(centerY(blob[1])/centerX(blob[0])))
    if centerX(blob[0]) < 0:
        angleOut = angleOut + 180
    return (angleOut + 360) % 360

def angleBetween(blob1, blob2):
    angleOut = angleTo(blob1) + angleTo(blob2)
    angleOut = angleOut / 2
    if abs(angleTo(blob1) - angleTo(blob2)) > 180:
        angleOut = angleOut + 180
    return angleOut

def runAlgo(imgIn):
    blueXs = []
    blueYs = []
    yellowXs = []
    yellowYs = []
    avgBlueBlob = (None,None)
    avgYellowBlob = (None,None)

    findblue = imgIn.find_blobs(
        [blue], pixels_threshold=PIX_THR, area_threshold=AREA_THR, merge=True)
    findyellow = imgIn.find_blobs(
        [yellow], pixels_threshold=PIX_THR, area_threshold=AREA_THR, merge=True)

    for blob in findblue:
        blueXs.append(blob.cx())
        blueYs.append(blob.cy())
        #draw_blob(blob, imgIn)
        #imgIn.draw_line(160,120,blob.cx(),blob.cy())

    for blob in findyellow:
        yellowXs.append(blob.cx())
        yellowYs.append(blob.cy())
        #draw_blob(blob, imgIn)
        #imgIn.draw_line(160,120,blob.cx(),blob.cy())

    imgIn.draw_rectangle(BLUE_REGION)
    imgIn.draw_rectangle(YELLOW_REGION)
    '''
    if len(blueXs) != 0:
        avgBlueBlob = (avg(blueXs), avg(blueYs))
        imgIn.draw_string(
           round(avgBlueBlob[0]) + 30,
           round(avgBlueBlob[1]) + 20,
           str(trueAngle(angleTo(avgBlueBlob))),
           color=(255, 0, 0)
        )
        print(trueAngle(angleTo(avgBlueBlob)))

    if len(yellowXs) != 0:
        avgYellowBlob = (avg(yellowXs), avg(yellowYs))
        print(trueAngle(angleTo(avgYellowBlob)))
        imgIn.draw_string(
            round(avgYellowBlob[0]) + 30,
            round(avgYellowBlob[1]) + 20,
            str(trueAngle(angleTo(avgYellowBlob))),
            color=(255, 0, 0)
        )
        '''
    return avgBlueBlob, avgYellowBlob

# Allocate a small buffer to allow errors to be logged with more detail
micropython.alloc_emergency_exception_buf(100)

# uart is the connection to the Teensy
uart = UART(3, 19200)
# clock is an object that keeps track of FPS
clock = time.clock()

# resets the camera and intializes it
sensor.reset()

# Set pixel format to RGB565
sensor.set_pixformat(sensor.RGB565)

# Set frame size to QVGA (320x240)
sensor.set_framesize(sensor.QVGA)

sensor.set_contrast(-1)
sensor.set_saturation(3)
sensor.set_brightness(0)

# Delay 2 seconds for setup
sensor.skip_frames(time=2000)

# Make camera automatically adjust image
setCameraAdjust(True)

# Take a picture from camera
takePic()

# After the first image was taken, turn the image adjustment off
setCameraAdjust(False)

# Gets the blue region of the
bluecall = img.get_statistics(roi=BLUE_REGION)
print("found the blue")
yellowcall = img.get_statistics(roi=YELLOW_REGION)
print("found the yellow")

# Means for the yellow region in LAB format
Y_LMean = yellowcall.l_mean()
Y_AMean = yellowcall.a_mean()
Y_BMean = yellowcall.b_mean()

# Means for the blue region in LAB format
B_LMean = bluecall.l_mean()
B_AMean = bluecall.a_mean()
B_BMean = bluecall.b_mean()

yellow = (
    (Y_LMean - BLOBDEV_L), (Y_LMean + BLOBDEV_L),
    (Y_AMean - BLOBDEV_A), (Y_AMean + BLOBDEV_A),
    (Y_BMean - BLOBDEV_B), (Y_BMean + BLOBDEV_B)
)

blue = (
    (B_LMean - BLOBDEV_L), (B_LMean + BLOBDEV_L),
    (B_AMean - BLOBDEV_A), (B_AMean + BLOBDEV_A),
    (B_BMean - BLOBDEV_B), (B_BMean + BLOBDEV_B)
)

thresholds = [
    yellow,
    blue,
]

time.sleep(2000)

# print the color thresholds for the two fields
print(thresholds[0], thresholds[1])

img = sensor.snapshot()

# Main loop
while(True):
    # if there are no blobs, just ignore the rest of the loop
    if blueXs and yellowXs and pin0.value() == 0 and mode = 0:
        takePic()
        img.save("img%d.jpg" % picnumber)
        picnumber += 1
    if mode = 1:
        pull = 0
        results = []
        readiterative = 0
        while True:
            try:
                img = image.Image("img%d.bmp" % pull, copy_to_fb = True)
                pull += 1
                results.append(runAlgo(img))
            except error as err:
                results.save("report.txt")
                pyb.exit()
    clock.tick()
