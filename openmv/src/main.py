# WARNING: based codebase ahead. Proceed with caution.

import sensor
import image
import time
import math
import ustruct
import micropython
import pyb
from pyb import UART

# TODO : Implement Absolute Location
# FIELD_WIDTH = 200
# FIELD_HEIGHT = 300

# The regions of interest to get colors for (sections of the camera's image to find colors for)
BLUE_REGION = (160, 5, 30, 5)
YELLOW_REGION = (160, 230, 30, 5)

# How far blobs are allowed to deviate from the specified color
BLOBDEV = 9

EDGE_COLOR = (255, 0, 0)
MAJOR_AXIS_COLOR = (0, 255, 0)
MINOR_AXIS_COLOR = (0, 0, 255)


def centerX(x):
    return x - 160


def centerY(y):
    return y - 120


# avg returns the average of a list's values. O(n)!
def avg(list):
    return sum(list) / len(list)


# toLAB converts RGB thresholds to LAB. Necessary for the camera
def toLAB(r, g, b, pm=20, lm=25):
    l, a, b = image.rgb_to_lab((r, g, b))
    return (l - lm, l + lm, a - pm, a + pm, b - pm, b + pm)


# setCameraAdjust sets the auto exposure, white balance, and gain variables on or off
def setCameraAdjust(value):
    sensor.set_auto_exposure(value)
    sensor.set_auto_whitebal(value)
    sensor.set_auto_gain(value)


def draw_blob(blob, img):
    img.draw_edges(blob.min_corners(), color=EDGE_COLOR)
    img.draw_line(blob.major_axis_line(), color=MAJOR_AXIS_COLOR)
    img.draw_line(blob.minor_axis_line(), color=MINOR_AXIS_COLOR)
    img.draw_rectangle(blob.rect())
    img.draw_cross(blob.cx(), blob.cy())
    img.draw_keypoints(
        [(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20
    )
    img.draw_rectangle(blob.rect())
    img.draw_cross(blob.cx(), blob.cy())
    img.draw_keypoints(
        [(blob.cx(), blob.cy(), int(math.degrees(blob.rotation())))], size=20
    )


# sendToTeensy any amount of data to the teensy
def sendToTeensy(comm, *data):
    comm.send("|".join(data) + "\n")


def angleBetween(vx1, vy1, vx2, vy2):
    ang1 = math.atan(centerY(vy1) / centerX(vx1))
    ang2 = math.atan(centerY(vy2) / centerX(vx2))
    return (math.degrees(ang1 + ang2)) / 2


def dirTo(x, y):
    return math.degrees(math.atan(Cy / x))


def angleToEnemy():
    return math.degrees(math.atan(blob.cy(), blob.cx()))


# Allocate a small buffer to allow errors to be logged with more detail
micropython.alloc_emergency_exception_buf(100)

# uart is the connection to the Teensy
uart = UART(3, 19200)
# clock is an object that keeps track of FPS
clock = time.clock()

# resets the camera and intializes it
sensor.reset()
# Set pixel format to RGB565
sensor.set_pixformat(sensor.RGB565)
# Set frame size to QVGA (320x240)
sensor.set_framesize(sensor.QVGA)
# Delay 2 seconds for setup
sensor.skip_frames(time=2000)
# Make camera automatically adjust image
setCameraAdjust(True)
# Take a picture from camera
img = sensor.snapshot()
# After the first image was taken, turn the image adjustment off
setCameraAdjust(False)
# Gets the blue region of the
bluecall = img.get_statistics(roi=BLUE_REGION)
print("found the blue")
yellowcall = img.get_statistics(roi=YELLOW_REGION)
print("found the yellow")

# Modes for the yellow region in LAB format
Y_LMode = yellowcall.l_mode()
Y_AMode = yellowcall.a_mode()
Y_BMode = yellowcall.b_mode()

# Modes for the blue region in LAB format
B_LMode = bluecall.l_mode()
B_AMode = bluecall.a_mode()
B_BMode = bluecall.b_mode()

yellow = (
    (Y_LMode - BLOBDEV),
    (Y_LMode + BLOBDEV),
    (Y_AMode - BLOBDEV),
    (Y_AMode + BLOBDEV),
    (Y_BMode - BLOBDEV),
    (Y_BMode + BLOBDEV),
)

blue = (
    (B_LMode - BLOBDEV),
    (B_LMode + BLOBDEV),
    (B_AMode - BLOBDEV),
    (B_AMode + BLOBDEV),
    (B_BMode - BLOBDEV),
    (B_BMode + BLOBDEV),
)

thresholds = [
    yellow,
    blue,
]

time.sleep(2000)

# print the color thresholds for the two fields
print(thresholds[0], thresholds[1])

# Main loop
while True:
    blueXs = []
    blueYs = []
    yellowXs = []
    yellowYs = []

    img = sensor.snapshot()

    findblue = img.find_blobs(
        [blue], pixels_threshold=200, area_threshold=400, merge=True
    )

    findyellow = img.find_blobs(
        [yellow], pixels_threshold=200, area_threshold=400, merge=True
    )

    for blob in findblue:
        blueXs.append(blob.cx())
        blueYs.append(blob.cy())
        draw_blob(blob, img)

    for blob in findyellow:
        yellowXs.append(blob.cx())
        yellowYs.append(blob.cy())
        draw_blob(blob, img)

    img.draw_rectangle(BLUE_REGION)
    img.draw_rectangle(YELLOW_REGION)

    # if there are no blobs, just ignore the rest of the loop
    if len(blueXs) == 0 and len(yellowXs) == 0:
        pass

    sendToTeensy(
        uart,
        angleBetween(avg(blueXs), avg(blueYs), avg(yellowXs), avg(yellowYs)),
        dirTo(avg(blueXs), avg(blueXs)),
        dirTo(avg(yellowXs), avg(yellowYs)),
    )
    # indicate that the camera finished this frame
    clock.tick()
