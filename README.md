# IMPORTANT : Read LICENSE.md for guidelines on fair use!

## Required Tools

- Visual Studio Code
- PlatformIO VSCode Extension

## Merge Requests

Merge requests are allowed, and even encouraged. Fork the repository using the 'fork' button in Gitlab, and clone the fork. Afterward, add your changes to your fork, commit them, and open a Merge Request on gitlab.
