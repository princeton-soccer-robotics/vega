#pragma once
#include <Arduino.h>

namespace polaris {
template<typename T>
String arrayRepr(T* vals, int size) {
	String out = "";
	for (int i = 0; i < size; i++) {
		out += " " + String(vals[i]);
	}
	return out;
}
}