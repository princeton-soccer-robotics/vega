#pragma once
#include <math.h>
#include <Arduino.h>

namespace polaris {
// forward declaration to make dumb compiler know these exist
struct AngleDeg;
struct Vec2;

bool areEqual(double a, double b);

struct AngleRad {
  float value;

  AngleDeg toDeg();
  Vec2 toVec();
};

struct AngleDeg {
  float value;

  AngleDeg(float value);

  AngleDeg operator+(double a);
  AngleDeg operator-(double a);
  AngleDeg operator*(double a);
  AngleDeg operator+=(const double other);
  AngleDeg operator-=(const double other);
  void operator=(double other);

  AngleRad toRad();
  AngleDeg trueAngle();
  AngleDeg angleDiff(AngleDeg other);
  Vec2 toVec();
};

struct Vec2 {
  float x;
  float y;

  Vec2(float x, float y);
  Vec2();

  Vec2 operator+(Vec2 const& a);
  Vec2 operator-(Vec2 const& a);
  Vec2 operator*(float const& f);
  Vec2 operator/(float const& f);
  Vec2 operator+=(const Vec2 other);
  Vec2 operator-=(const Vec2 other);
  void operator=(Vec2 const& other);

  float magnitude();
  AngleRad toAngleRad();
  AngleDeg toAngleDeg();
  Vec2 toUnitVector();
  Vec2 flipped();
  bool isZero();
  String toString();
};
};  // namespace polaris