#pragma once
#include <Arduino.h>
#include <Config.h>

#ifdef USE_TEENSYTHREADS
#include <TeensyThreads.h>

typedef Threads::Grab<decltype(Serial)> serial_wrapped_t;
typedef Threads::Grab<decltype(Serial1)> serial1_wrapped_t;
typedef Threads::Grab<decltype(Serial2)> serial2_wrapped_t;
typedef Threads::Grab<decltype(Serial3)> serial3_wrapped_t;
typedef Threads::Grab<decltype(Serial4)> serial4_wrapped_t;
typedef Threads::Grab<decltype(Serial5)> serial5_wrapped_t;
typedef Threads::Grab<decltype(Serial6)> serial6_wrapped_t;
typedef Threads::Grab<decltype(Serial7)> serial7_wrapped_t;
typedef Threads::Grab<decltype(Serial8)> serial8_wrapped_t;

serial_wrapped_t SerialWrapped(Serial);
serial1_wrapped_t Serial1Wrapped(Serial1);
serial2_wrapped_t Serial2Wrapped(Serial2);
serial3_wrapped_t Serial3Wrapped(Serial3);
serial4_wrapped_t Serial4Wrapped(Serial4);
serial5_wrapped_t Serial5Wrapped(Serial5);
serial6_wrapped_t Serial6Wrapped(Serial6);
serial7_wrapped_t Serial7Wrapped(Serial7);
serial8_wrapped_t Serial8Wrapped(Serial8);

#define PSerial ThreadClone(SerialWrapped)
#define PSerial1 ThreadClone(Serial1Wrapped)
#define PSerial2 ThreadClone(Serial2Wrapped)
#define PSerial3 ThreadClone(Serial3Wrapped)
#define PSerial4 ThreadClone(Serial4Wrapped)
#define PSerial5 ThreadClone(Serial5Wrapped)
#define PSerial6 ThreadClone(Serial6Wrapped)
#define PSerial7 ThreadClone(Serial7Wrapped)
#define PSerial8 ThreadClone(Serial8Wrapped)

#else
#define PSerial Serial
#define PSerial1 Serial1
#define PSerial2 Serial2
#define PSerial3 Serial3
#define PSerial4 Serial4
#define PSerial5 Serial5
#define PSerial6 Serial6
#define PSerial7 Serial7
#define PSerial8 Serial8
#endif