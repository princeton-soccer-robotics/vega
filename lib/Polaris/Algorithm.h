#pragma once
#include <PMath.h>

namespace polaris {
// A utility function to swap two elements
void swap(int* a, int* b);

/* This function takes last element as pivot, places
the pivot element at its correct position in sorted
array, and places all smaller (smaller than pivot)
to left of pivot and all greater elements to right
of pivot */
int partition(int arr[], int indices[], int low, int high);

/* The main function that implements QuickSort
arr[] --> Array to be sorted,
indices[] --> Array of indices to keep track of
low --> Starting index,
high --> Ending index */
void quickSort(int arr[], int indices[], int low, int high);

float dotProduct(Vec2 a, Vec2 b);
}