#include <Algorithm.h>

void polaris::swap(int* a, int* b) {
  int t = *a;
  *a = *b;
  *b = t;
}

int polaris::partition(int arr[], int indices[], int low, int high) {
  int pivot = arr[high];  // pivot
  int i = (low - 1);      // Index of smaller element

  for (int j = low; j <= high - 1; j++) {
    // If current element is smaller than the pivot
    if (arr[j] < pivot) {
      i++;  // increment index of smaller element
      swap(&indices[i], &indices[j]);
      swap(&arr[i], &arr[j]);
    }
  }
  swap(&indices[i + 1], &indices[high]);
  swap(&arr[i + 1], &arr[high]);
  return (i + 1);
}

void polaris::quickSort(int arr[], int indices[], int low, int high) {
  if (low < high) {
    /* pi is partitioning index, arr[p] is now
    at right place */
    int pi = partition(arr, indices, low, high);

    // Separately sort elements before
    // partition and after partition
    quickSort(arr, indices, low, pi - 1);
    quickSort(arr, indices, pi + 1, high);
  }
}

float polaris::dotProduct(Vec2 a, Vec2 b) {
  return (a.x * b.x) + (a.y * b.y);
}