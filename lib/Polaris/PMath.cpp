#include <PMath.h>

namespace polaris {

bool areEqual(double a, double b) {
  return fabs(a - b) < 0.002;
}

AngleDeg AngleRad::toDeg() {
  return AngleDeg{this->value * (180.0 / M_PI)};
}

AngleDeg::AngleDeg(float value) {
  this->value = value;
}

AngleRad AngleDeg::toRad() {
  return AngleRad{this->value * (M_PI / 180.0)};
}

AngleDeg AngleDeg::trueAngle() {
  return AngleDeg{fmod((450.0 - this->value), 360.0)};
}

AngleDeg AngleDeg::angleDiff(AngleDeg other) {
  auto angle1 = this->value;
  auto angle2 = other.value;
  if (angle1 < 0)
    angle1 += 360.0;
  if (angle2 < 0)
    angle2 += 360.0;
  float diff = fmod((angle2 - angle1 + 180), 360) - 180;
  return AngleDeg{diff < -180 ? diff + 360 : diff};
}

Vec2 AngleDeg::toVec() {
  return Vec2(cos(AngleDeg(value).toRad().value),
              sin(AngleDeg(value).toRad().value));
}

Vec2::Vec2(float x, float y) {
  this->x = x;
  this->y = y;
}

Vec2::Vec2() {
  this->x = 0.0;
  this->y = 0.0;
}

#pragma region NERDY_SHIT
AngleDeg AngleDeg::operator+(double a) {
  return AngleDeg(value + a);
}

AngleDeg AngleDeg::operator-(double a) {
  return AngleDeg(value - a);
}

AngleDeg AngleDeg::operator*(double a) {
  return AngleDeg(value * a);
}

AngleDeg AngleDeg::operator+=(const double other) {
  value += other;
  return *this;
}

AngleDeg AngleDeg::operator-=(const double other) {
  value -= other;
  return *this;
}

void AngleDeg::operator=(double other) {
  value = other;
}

Vec2 Vec2::operator+(Vec2 const& a) {
  return Vec2(x + a.x, y + a.y);
}

Vec2 Vec2::operator-(Vec2 const& a) {
  return Vec2(x - a.x, y - a.y);
}

Vec2 Vec2::operator*(float const& f) {
  return Vec2(x * f, y * f);
}

Vec2 Vec2::operator/(float const& f) {
  return Vec2(x / f, y / f);
}

Vec2 Vec2::operator+=(const Vec2 other) {
  x = x + other.x;
  y = y + other.y;
  return *this;
}

Vec2 Vec2::operator-=(const Vec2 other) {
  x = x - other.x;
  y = y - other.y;
  return *this;
}

void Vec2::operator=(Vec2 const& other) {
  this->x = other.x;
  this->y = other.y;
}
#pragma endregion

float Vec2::magnitude() {
  return sqrt((x * x) + (y * y));
}

AngleRad Vec2::toAngleRad() {
  return AngleRad{atan2(y, x)};
}

AngleDeg Vec2::toAngleDeg() {
  return this->toAngleRad().toDeg();
}

Vec2 Vec2::toUnitVector() {
  const float magnitude = this->magnitude();
  if(fabs(x) < 0.01 && fabs(y) < 0.01){
    return Vec2(0, 0);
  }
  return Vec2(this->x / magnitude, this->y / magnitude);
}

Vec2 Vec2::flipped() {
  return Vec2(-this->x, -this->y);
}

bool Vec2::isZero() {
  return (fabs(x) < 0.01 && fabs(y) < 0.01);
}

String Vec2::toString() {
  return "{ " + String(this->x) + ", " + String(this->y) + " }";
}
};  // namespace polaris